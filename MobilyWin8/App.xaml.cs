﻿using MobilyWin8.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MobilyWin8.Views;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation; 
using Windows.UI.Core;
using System.Threading.Tasks;
using MobilyWin8.Services;

// The Grid App template is documented at http://go.microsoft.com/fwlink/?LinkId=234226

namespace MobilyWin8
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        private Shell shell;
         
         

        public static ViewModelLocator ViewModelLocator
        {
            get { return (ViewModelLocator)Current.Resources["ViewModelLocator"]; }
        }

        public static CoreDispatcher Dispatcher { get; set; }



        /// <summary>
        /// Initializes the singleton Application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();

            UnhandledException += OnUnhandledException;
        }
        void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            OnUnhandledException(e.Exception);
        }

        public async void OnUnhandledException(Exception exception)
        {
            var dispatcher = App.Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, (DispatchedHandler)(async () =>
            {
#if DEBUG
                var baseEx = exception.GetBaseException();
                var message = exception.Message;
                if (baseEx != exception)
                {
                    message += "\r\n\r\n" + baseEx.Message;
                }
                await ViewModelLocator.DialogService.ShowMessageAsync(message);
#else
                await ViewModelLocator.DialogService.ShowResourceMessageAsync("Exception_UnhandledException");
                Exit();
#endif
            }));
        }
        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            try
            {
                await EnsureShell(args.PreviousExecutionState);
                ViewModelLocator.NavigationService.Navigate(typeof(MainPage));
            }
            catch (Exception ex)
            {
                // Due to an issues I've noted online: http://social.msdn.microsoft.com/Forums/en/winappswithcsharp/thread/bea154b0-08b0-4fdc-be31-058d9f5d1c4e
                // I am limiting the use of 'async void'  In a few rare occasions I use it
                // and manually route the exceptions to the UnhandledExceptionHandler
                ((App)App.Current).OnUnhandledException(ex);
            }
        }

        private async Task EnsureShell(ApplicationExecutionState previousState)
        {
            if (previousState == ApplicationExecutionState.Running)
            {
                Window.Current.Activate();
                ViewModelLocator.NavigationService.InitializeFrame(shell.Frame);
                return;
            }

            if (previousState == ApplicationExecutionState.Terminated || previousState == ApplicationExecutionState.ClosedByUser)
            {
                var settings = ViewModelLocator.Container.Resolve<ApplicationSettings>();
                await settings.RestoreAsync();
            }

            shell = new Shell();
            ViewModelLocator.NavigationService.InitializeFrame(shell.Frame);
            Window.Current.Content = shell;
            Window.Current.Activate();
            Dispatcher = Window.Current.CoreWindow.Dispatcher;

           // MessagePumps.StartAll();

            ViewModelLocator.ShellViewModel.IsLoading = false;
        }
    }
}
