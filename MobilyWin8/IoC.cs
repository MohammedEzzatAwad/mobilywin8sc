﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroIoc;
using MobilyWin8.Adapters;
using MobilyWin8.Messaging;
using MobilyWin8.Messaging.Handlers;
using MobilyWin8.Services;
using MobilyWin8.ViewModels;
 
namespace MobilyWin8
{
    public class IoC
    {
        public static IContainer BuildContainer() 
        {
            var container = new MetroContainer();
            container.RegisterInstance(container);
            container.RegisterInstance<IContainer>(container);

            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                container.Register<IBackgroundDownloader, NullBackgroundDownloader>();
                container.RegisterInstance<ApplicationSettings>(new NullApplicationSettings());
            }
            else 
            {
                container.Register<IBackgroundDownloader, BackgroundDownloaderAdapter>();
                container.RegisterInstance<ApplicationSettings>(new ApplicationSettings(container.Resolve<IBackgroundDownloader>()));
                
            }
            container.RegisterInstance<IDataTransferManager>(new DataTransferManagerAdapter());
            container.Register<INavigationService, NavigationService>(lifecycle: new SingletonLifecycle());
            container.Register<IHub, MessageHub>(lifecycle: new SingletonLifecycle());
            container.Register<IDialogService, DialogService>();
            container.Register<ITileUpdateManager, TileUpdateManagerAdapter>();
            container.Register<IToastNotificationManager, ToastNotificationManagerAdapter>();
            
            RegisterHandlers(container);

            container.Register<ShellViewModel>(lifecycle: new SingletonLifecycle());
            // I would prefer not to immediately resolve a singleton, but the MetroContainer doesn't support resolving from Method or provider alternatives.
            container.RegisterInstance<IStatusService>(container.Resolve<ShellViewModel>());
 
            return container;
        }

        private static void RegisterHandlers(IContainer container)
        {
            //TODO: Add Assembly Scanning here to auto-register
            container.Register<IHandler<ShowSettingsMessage>, ShowSettingsHandler>();
            container.Register<IHandler<LogInLogOutMessage>, LogInLogOutHandler>();
        }
    }
}
