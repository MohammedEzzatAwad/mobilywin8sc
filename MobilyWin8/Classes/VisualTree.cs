﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace MobilyWin8.Classes
{
    public static class VisualTree
    {
        // Methods
        public static T FindVisualChild<T>(this DependencyObject obj) where T : DependencyObject
        {
            if (obj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                    if ((child != null) && (child is T))
                    {
                        return (T)child;
                    }
                    T local = child.FindVisualChild<T>();
                    if (local != null)
                    {
                        return local;
                    }
                }
            }
            return default(T);
        }

        public static T FindVisualParent<T>(this DependencyObject obj) where T : DependencyObject
        {
            if (obj != null)
            {
                for (DependencyObject obj2 = VisualTreeHelper.GetParent(obj); obj2 != null; obj2 = VisualTreeHelper.GetParent(obj2))
                {
                    T local = obj2 as T;
                    if (local != null)
                    {
                        return local;
                    }
                }
            }
            return default(T);
        }

        /// <summary>
        /// Gets the Visual Tree for a DependencyObject with that DependencyObject as the root.
        /// </summary>
        /// <param name="element">The root element.</param>
        /// <returns>The matching elements.</returns>
        public static IEnumerable<DependencyObject> GetVisualTree(this DependencyObject element)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(element);

            for (int i = 0; i < childrenCount; i++)
            {
                var visualChild = VisualTreeHelper.GetChild(element, i);

                yield return visualChild;

                foreach (var visualChildren in GetVisualTree(visualChild))
                {
                    yield return visualChildren;
                }
            }
        }

    }
}
