﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilyWin8.Services
{
    public interface IDialogService
    {
        Task ShowMessageAsync(string message);
        Task ShowResourceMessageAsync(string key);
    }
}
