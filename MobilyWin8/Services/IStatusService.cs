﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobilyWin8.ViewModels;

namespace MobilyWin8.Services
{
    public interface IStatusService
    {
        bool IsLoading { get; set; }
        string Message { get; set; }
        string TemporaryMessage { get; set; }
        PersonVM PersonVM { get; set; }
        string PageTitle { get; set; }
        bool IsLoggedIn { get; set; } 
        void LogIn(string userName, string password);
        void SetNetworkUnavailable();
        void SetMobilyUnavailable();
    }
}
