﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilyWin8.Services
{
    public interface ISuspensionManager
    {
        Dictionary<string, object> SessionState { get; }
        List<Type> KnownTypes { get; }
        Task SaveAsync();
        Task RestoreAsync();
    }
}
