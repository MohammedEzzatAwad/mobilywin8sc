﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace MobilyWin8.Services
{
    public interface INavigationService
    {
        event NavigatingCancelEventHandler Navigating;

        void InitializeFrame(Frame frame);
        void Navigate(Type source, object parameter = null);
        bool CanGoBack { get; }
        void GoBack();
        void GoHome();
        ICommand GoBackCommand { get; }
    }
}
