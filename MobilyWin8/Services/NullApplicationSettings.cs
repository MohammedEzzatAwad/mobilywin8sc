﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobilyWin8.Adapters;
using MobilyWin8.ViewModels;

namespace MobilyWin8.Services
{
    public class NullApplicationSettings : ApplicationSettings
    {
        public NullApplicationSettings()
            : base(new NullSuspensionManager(), new NullBackgroundDownloader(), null)
        {
        }

        class NullSuspensionManager : ISuspensionManager
        {
            public NullSuspensionManager()
            {
                SessionState = new Dictionary<string, object>();
                KnownTypes = new List<Type>();
            }

            public Dictionary<string, object> SessionState { get; private set; }
            public List<Type> KnownTypes { get; private set; }

            public async Task SaveAsync()
            {
                await Task.Run(() => { });
            }

            public async Task RestoreAsync()
            {
                await Task.Run(() => { });
            }
        }
    }
}
