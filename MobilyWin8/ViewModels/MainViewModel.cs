﻿using System.Windows.Input;
using MobilyWin8.Common;
using MobilyWin8.Messaging;
using MobilyWin8.Services;
using MobilyWin8.Views;
using Windows.UI.Popups;

namespace MobilyWin8.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private readonly ApplicationSettings _settings;
        private readonly INavigationService _navigationService;
        private readonly IHub _messageHub;
        private readonly IStatusService _statusService;

        public MainViewModel(ApplicationSettings settings, INavigationService navigationService, IHub messageHub, IStatusService statusService)
        {
            _settings = settings;
            _navigationService = navigationService;
            _messageHub = messageHub;
            _statusService = statusService;


            _statusService.PageTitle = "";
            SettingsCommand = new DelegateCommand(Settings);
            
            ProductsAndServiceCommand = new DelegateCommand(ProductsAndService);
            MyAccountCommand = new DelegateCommand(MyAccount);
            NotificationsAndUpdatesCommand = new DelegateCommand(NotificationsAndUpdates);
            Mobily3alhawaCommand = new DelegateCommand(Mobily3alhawa);
        }


        private PersonVM _personVM;
        public PersonVM PersonVM
        {
            get { return _settings.SelectedInstance; }
            set
            {
                SetProperty(ref _personVM, value);
            }
        }

        public ICommand SettingsCommand { get; set; }


        public void Settings()
        {
            _messageHub.Send(new ShowSettingsMessage());


            PersonVM = _statusService.PersonVM;
        }

        public ICommand NotificationsAndUpdatesCommand { get; set; }


        public void NotificationsAndUpdates()
        { 
            if (_statusService.IsLoggedIn)
                _navigationService.Navigate(typeof(NotificationsAndUpdatesPage));
            else
            {
                var md = new MessageDialog("You're not Logged In");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();
            }
        }

        public ICommand Mobily3alhawaCommand { get; set; }

        public void Mobily3alhawa()
        {
            if (_statusService.IsLoggedIn)
                _navigationService.Navigate(typeof(AlhawaNewsPage));
            else
            {
                var md = new MessageDialog("You're not Logged In");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();
            }
        }

        public ICommand ProductsAndServiceCommand { get; set; }


        public void ProductsAndService()
        {
            _navigationService.Navigate(typeof(ProductsAndServicesPage));
        }

        public ICommand MyAccountCommand { get; set; }


        public void MyAccount()
        {
            if (_statusService.IsLoggedIn)
                _navigationService.Navigate(typeof (MyAccountPage));
            else
            {
                var md = new MessageDialog("You're not Logged In");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();
            }
        }
    }
}