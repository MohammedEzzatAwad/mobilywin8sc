﻿using MobilyWin8.Common;
using MobilyWin8.Messaging;
using MobilyWin8.Services;

namespace MobilyWin8.ViewModels
{
    public class NotificationsAndUpdatesViewModel  : BindableBase
    {
        private readonly ApplicationSettings _settings;
        private readonly INavigationService _navigationService;
        private readonly IHub _messageHub;
        private readonly IStatusService _statusService;

        public NotificationsAndUpdatesViewModel(ApplicationSettings settings, INavigationService navigationService, IHub messageHub, IStatusService statusService)
        {
            _settings = settings;
            _navigationService = navigationService;
            _messageHub = messageHub;
            _statusService = statusService;

            _statusService.PageTitle = "Notification & Updates";

        }

        private PersonVM _personVM;
        public PersonVM PersonVM
        {
            get { return _settings.SelectedInstance; }
            set
            {
                SetProperty(ref _personVM, value);
            }
        }
    }
}