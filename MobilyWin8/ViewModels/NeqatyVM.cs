﻿using System;
using System.Globalization;
using System.Xml.Linq;
using MobilyAppWin8.Requests;
using MobilyWin8.Requests;

namespace MobilyWin8.ViewModels
{
    public class NeqatyVM : AbstractNeqatyVM 
    { 
        public async void PostNeqaty(string hashcode)
        {
            string msg = await 
                BalanceBillRequest.LoginToEPortal(LoginRequest.CreateXmlForBalanceBillNeqaty(hashcode,
                                                                                             Constants.neqaty_LOYALTY_VIEW_SUMMARY));
            msg = msg.Replace("---!>", "-->");
            string error = ConstantsRequest.GetError(msg);
            if (error == null || error == string.Empty)
                this.ParseXml(msg);
            else
                this.RaiseErrorFound("Neqaty :" + error);
        }


        private void ParseXml(string msg)
        {
            CultureInfo invariantCulture = CultureInfo.InvariantCulture;
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        //if (xelement2.Name == (XName)Constants.neqaty_LOYALTY_INQUIRY_ITEMS)
                        //{
                             
                                
                                if (xelement2.Name == (XName)Constants.neqaty_CurrentBalance)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.CurrentBalance =xelement2.Value;
                                                                        
                                } 
                                else if(xelement2.Name == (XName)Constants.neqaty_EarnedPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.EarnedPoints = xelement2.Value;
                                                                       
                                }
                                else if (xelement2.Name == (XName)Constants.neqaty_ExpirySchedulerPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.ExpirySchedulePoints = xelement2.Value;
                                }
                                else if (xelement2.Name == (XName)Constants.neqaty_LostPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.LostPoints = xelement2.Value;
                                }
                                else if (xelement2.Name == (XName)Constants.neqaty_RedeemedPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.RedeemedPoints = xelement2.Value;
                                }
                            }
                        //}
                    //}
                }
            }
        }
    }
}