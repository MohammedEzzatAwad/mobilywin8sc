﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MobilyWin8.Common;
using MobilyWin8.Helpers;
using MobilyWin8.Models;
using Windows.UI.Xaml.Media.Imaging;

namespace MobilyWin8.ViewModels
{
    public class AbstractNewsAndPromotionsVM :BindableBase
    {
        protected ObservableCollection<NewsPromotionItem> m_refNewsColl = (ObservableCollection<NewsPromotionItem>)null;
        protected ObservableCollection<NewsPromotionItem> m_refPromotionColl = (ObservableCollection<NewsPromotionItem>)null;
        protected int _count = 0;

        protected int Counts
        {
            get { return m_refNewsColl.Count + m_refPromotionColl.Count; }
             
        }
        private PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> collNewsChanged;
        private PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> collPromotionChanged;
 
        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;

        public virtual ObservableCollection<NewsPromotionItem> NewsColl
        {
            get
            {
                return this.m_refNewsColl;
            }
            set
            {
                ObservableCollection<NewsPromotionItem> refOldValue = this.m_refNewsColl;
                if (!this.ValidatePromotionCollChange(refOldValue, value))
                    return;
                this.SetProperty(ref m_refNewsColl, value);
                this.HandlePromotionCollChanged(refOldValue, this.m_refNewsColl);
                this.RaiseCollPromotionChanged(refOldValue, this.m_refNewsColl);
            }
        }

        public virtual ObservableCollection<NewsPromotionItem> PromotionColl
        {
            get
            {
                return this.m_refPromotionColl;
            }
            set
            {
                ObservableCollection<NewsPromotionItem> refOldValue = this.m_refPromotionColl;
                if (!this.ValidateNewsCollChange(refOldValue, value))
                    return;
                this.SetProperty(ref m_refPromotionColl, value);
                this.HandleNewsCollChanged(refOldValue, this.m_refPromotionColl);
                this.RaiseCollNewsChanged(refOldValue, this.m_refPromotionColl);
            }
        }


        public event PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> CollNewsChanged
        {
            add
            {
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> changedEventHandler = this.collNewsChanged;
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>>>(ref this.collNewsChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> changedEventHandler = this.collNewsChanged;
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>>>(ref this.collNewsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> CollPromotionChanged
        {
            add
            {
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> changedEventHandler = this.collPromotionChanged;
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>>>(ref this.collPromotionChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> changedEventHandler = this.collPromotionChanged;
                PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<NewsPromotionItem>>>(ref this.collNewsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }


        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected virtual void HandleNewsCollChanged(ObservableCollection<NewsPromotionItem> refOldValue, ObservableCollection<NewsPromotionItem> refNewValue)
        {
        }

        protected virtual bool ValidateNewsCollChange(ObservableCollection<NewsPromotionItem> refOldValue, ObservableCollection<NewsPromotionItem> refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }


        protected void RaiseCollNewsChanged(ObservableCollection<NewsPromotionItem> refOldValue, ObservableCollection<NewsPromotionItem> refNewValue)
        {
            if (this.collNewsChanged == null)
                return;
            this.collNewsChanged((object)this, new PropertyChangedEventArgs<ObservableCollection<NewsPromotionItem>>(refOldValue, refNewValue));
        }

        protected virtual void HandlePromotionCollChanged(ObservableCollection<NewsPromotionItem> refOldValue, ObservableCollection<NewsPromotionItem> refNewValue)
        {
        }

        protected virtual bool ValidatePromotionCollChange(ObservableCollection<NewsPromotionItem> refOldValue, ObservableCollection<NewsPromotionItem> refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }


        protected void RaiseCollPromotionChanged(ObservableCollection<NewsPromotionItem> refOldValue, ObservableCollection<NewsPromotionItem> refNewValue)
        {
            if (this.collNewsChanged == null)
                return;
            this.collNewsChanged((object)this, new PropertyChangedEventArgs<ObservableCollection<NewsPromotionItem>>(refOldValue, refNewValue));
        }
        protected void RaiseErrorFound(string errorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(errorName));
        }
        protected void RaiseProgress(string Where)
        {
            if (this.progress == null)
                return;
            this.progress((object)this, new GenericEventArgs<string>(Where));
        }

    }

  
}
