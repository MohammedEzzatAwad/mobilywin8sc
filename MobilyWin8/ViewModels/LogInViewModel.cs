﻿ 
using System.Threading.Tasks;
using System.Windows.Input;
using MobilyAppWin8.Requests;
using MobilyWin8.Adapters;
using MobilyWin8.Common;
using MobilyWin8.Messaging;
using MobilyWin8.Requests;
using MobilyWin8.Services; 

namespace MobilyWin8.ViewModels
{
    public class LogInViewModel : BindableBase
    {
        private readonly ApplicationSettings _settings;
        private readonly IHub _messageHub;
        private readonly IStatusService _statusService;
        public LogInViewModel(ApplicationSettings settings  ,IHub messageHub , IStatusService statusService)
        {
            _settings = settings;
            _messageHub = messageHub;
             _statusService = statusService;
            LogInCommand = new AsyncDelegateCommand(LogIn);

            this.UserName = "freesoul";
            this.Password = "Nuitex2012";
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            { 
                    SetProperty(ref _userName, value); 
            }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set
            { 
                 SetProperty(ref _password, value);
                  }
        }

        private bool _isLoggedIn;
        public bool IsLoggedIn
        {
            get { return _isLoggedIn; }
            set { SetProperty(ref _isLoggedIn, value); }
        }

        public ICommand LogInCommand { get; set; }

        public async Task LogIn()
        {
             
            _statusService.LogIn(UserName,Password);
        }
    }
}
