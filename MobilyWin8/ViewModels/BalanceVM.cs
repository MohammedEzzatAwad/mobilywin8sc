﻿using System;
using System.Globalization;
using System.Xml.Linq;
using MobilyAppWin8.Requests;
using MobilyWin8.Requests;

namespace MobilyWin8.ViewModels
{
    public class BalanceVM : AbstractBalanceVM
    {
        public async void PostBalance(string hashcode)
        {
            string msg = await BalanceBillRequest.LoginToEPortal(LoginRequest.CreateXmlForBalanceBillNeqaty(hashcode, Constants.balancebill_GET_BALANCE));
            msg = msg.Replace("---!>", "-->");
            string error = ConstantsRequest.GetError(msg);
            if (error == null || error == string.Empty)
                this.ParseXml(msg);
            else
                this.RaiseErrorFound("Balance :" + error);
        }

        private void ParseXml(string msg)
        {
            CultureInfo invariantCulture = CultureInfo.InvariantCulture;
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.balancebill_BALANCE)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.balancebill_LINENUMBER)
                                    this.LineNumber = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_EXPIRATION_DATE)
                                    this.ExpirationDate = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_BALANCE)
                                    this.Balance = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_UnbilledAmount)
                                    this.UnbilledAmount = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_DueAmount)
                                    this.DueAmount = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeMinutes)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeMinutes = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeOnNetMinutes)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeOnNetMinutes = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeSMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeSMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeOnNetSMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeOnNetSMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeMMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeMMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeOnNetMMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeOnNetMMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeGPRS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeGPRS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_NationalFavoriteNumber)
                                    this.NationalFavoriteNumber = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_InternationalFavoriteNumber)
                                    this.InternationalFavoriteNumber = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}