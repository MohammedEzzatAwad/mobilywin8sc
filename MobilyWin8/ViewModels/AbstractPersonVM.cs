﻿using System;
using System.Threading;
using MobilyWin8.Common;
using MobilyWin8.Helpers;

namespace MobilyWin8.ViewModels
{
    public abstract class AbstractPersonVM : BindableBase
    {
        protected BillVM m_refBill = (BillVM)null;
        protected BalanceVM m_refBalance = (BalanceVM)null;
        protected NeqatyVM m_refNeqaty = (NeqatyVM)null;
        protected SupplementaryServiceVM m_refSupplementaryService = (SupplementaryServiceVM) null;
        protected NewsAndPromotionsVM m_refNewsAndPromotions = (NewsAndPromotionsVM)null;
        protected AlhawaVM m_refAlhawaNews = null;

        protected string m_refHashCode = "";
        private PropertyChangedEventHandler<BillVM> billChanged;
        private PropertyChangedEventHandler<BalanceVM> balanceChanged;
        private PropertyChangedEventHandler<NeqatyVM> neqatyChanged;
        private PropertyChangedEventHandler<SupplementaryServiceVM> supplementaryServiceChanged;
        private PropertyChangedEventHandler<NewsAndPromotionsVM> newsAndPromotioneChanged;
        private PropertyChangedEventHandler<AlhawaVM> alhawaNewsChanged; 
        private PropertyChangedEventHandler<string> hashCodeChanged;
        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;


        public virtual BillVM Bill
        {
            get
            {
                return this.m_refBill;
            }
            set
            {
                BillVM refOldValue = this.m_refBill;
                if (!this.ValidateBillChange(refOldValue, value))
                    return;
                SetProperty(ref m_refBill, value);
                this.HandleBillChanged(refOldValue, this.m_refBill);

                this.RaiseBillChanged(refOldValue, this.m_refBill);
            }
        }

        public virtual BalanceVM Balance
        {
            get
            {
                return this.m_refBalance;
            }
            set
            {
                BalanceVM refOldValue = this.m_refBalance;
                if (!this.ValidateBalanceChange(refOldValue, value))
                    return;
                SetProperty(ref m_refBalance, value);
                this.HandleBalanceChanged(refOldValue, this.m_refBalance);

                this.RaiseBalanceChanged(refOldValue, this.m_refBalance);
            }
        }

        public virtual NeqatyVM Neqaty
        {
            get { return this.m_refNeqaty; }
            set
            {
                NeqatyVM refOldValue = this.m_refNeqaty;
                if (!this.ValidateNeqatyChange(refOldValue, value))
                    return;
                SetProperty(ref m_refNeqaty, value);
                this.HandleNeqatyChanged(refOldValue, this.m_refNeqaty);

                this.RaiseNeqatyChanged(refOldValue, this.m_refNeqaty);
            }
        }

        public virtual SupplementaryServiceVM SupplementaryService
        {
            get { return m_refSupplementaryService; }
            set { 
                SupplementaryServiceVM refOldValue = this.m_refSupplementaryService;
            if(!this.ValidateSupplementaryServiceChange(refOldValue,value))
                return;
                SetProperty(ref m_refSupplementaryService, value);
                this.HandleSupplementaryServiceChanged(refOldValue, this.m_refSupplementaryService);

                this.RaiseSupplementaryServiceChanged(refOldValue,this.m_refSupplementaryService);
            }
        }

        public virtual NewsAndPromotionsVM NewsAndPromotions
        {
            get { return m_refNewsAndPromotions; }
            set
            {
                NewsAndPromotionsVM refOldValue = this.m_refNewsAndPromotions;
                if (!this.ValidateNewsAndPromotionChange(refOldValue, value))
                    return;
                SetProperty(ref m_refNewsAndPromotions, value);
                this.HandleNewsAndPromotionChanged(refOldValue, this.m_refNewsAndPromotions);

                this.RaiseNewsAndPromotionChanged(refOldValue, this.m_refNewsAndPromotions);
            }
        }

        public virtual AlhawaVM AlhwaNews
        {
            get { return m_refAlhawaNews; }
            set
            {
                AlhawaVM refOldValue = this.m_refAlhawaNews;
                if (!this.ValidateAlhawaNewsChange(refOldValue, value))
                    return;
                SetProperty(ref m_refAlhawaNews, value);
                this.HandleAlhawaNewsChanged(refOldValue, this.m_refAlhawaNews);

                this.RaiseAlhawaNewsChanged(refOldValue, this.m_refAlhawaNews);
            }
        }

        public virtual string HashCode
        {
            get
            {
                return this.m_refHashCode;
            }
            set
            {
                string refOldValue = this.m_refHashCode;
                if (!this.ValidateHashCodeChange(refOldValue, value))
                    return;
                SetProperty(ref this.m_refHashCode, value);
                this.HandleHashCodeChanged(refOldValue, this.m_refHashCode);

                this.RaiseHashCodeChanged(refOldValue, this.m_refHashCode);
            }
        }

        public event PropertyChangedEventHandler<BillVM> BillChanged
        {
            add
            {
                PropertyChangedEventHandler<BillVM> changedEventHandler = this.billChanged;
                PropertyChangedEventHandler<BillVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BillVM>>(ref this.billChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<BillVM> changedEventHandler = this.billChanged;
                PropertyChangedEventHandler<BillVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BillVM>>(ref this.billChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<BalanceVM> BalanceChanged
        {
            add
            {
                PropertyChangedEventHandler<BalanceVM> changedEventHandler = this.balanceChanged;
                PropertyChangedEventHandler<BalanceVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BalanceVM>>(ref this.balanceChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<BalanceVM> changedEventHandler = this.balanceChanged;
                PropertyChangedEventHandler<BalanceVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BalanceVM>>(ref this.balanceChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<NeqatyVM> NeqatyChanged
        {
            add
            {
                PropertyChangedEventHandler<NeqatyVM> changedEventHandler = this.neqatyChanged;
                PropertyChangedEventHandler<NeqatyVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<NeqatyVM>>(ref this.neqatyChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<NeqatyVM> changedEventHandler = this.neqatyChanged;
                PropertyChangedEventHandler<NeqatyVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<NeqatyVM>>(ref this.neqatyChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<AlhawaVM> AlhawaNewsChanged
        {
            add
            {
                PropertyChangedEventHandler<AlhawaVM> changedEventHandler = this.alhawaNewsChanged;
                PropertyChangedEventHandler<AlhawaVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<AlhawaVM>>(ref this.alhawaNewsChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<AlhawaVM> changedEventHandler = this.alhawaNewsChanged;
                PropertyChangedEventHandler<AlhawaVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<AlhawaVM>>(ref this.alhawaNewsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> HashCodeChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.hashCodeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.hashCodeChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.hashCodeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.hashCodeChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected void RaiseBillChanged(BillVM refOldValue, BillVM refNewValue)
        {
            if (this.billChanged == null)
                return;
            this.billChanged((object)this, new PropertyChangedEventArgs<BillVM>(refOldValue, refNewValue));
        }

        protected void RaiseBalanceChanged(BalanceVM refOldValue, BalanceVM refNewValue)
        {
            if (this.balanceChanged == null)
                return;
            this.balanceChanged((object)this, new PropertyChangedEventArgs<BalanceVM>(refOldValue, refNewValue));
        }
        protected void RaiseNeqatyChanged(NeqatyVM refOldValue, NeqatyVM refNewValue)
        {
            if (this.neqatyChanged == null)
                return;
            this.neqatyChanged((object)this, new PropertyChangedEventArgs<NeqatyVM>(refOldValue, refNewValue));
        }
        protected void RaiseSupplementaryServiceChanged(SupplementaryServiceVM refOldValue, SupplementaryServiceVM refNewValue)
        {
            if (this.supplementaryServiceChanged == null)
                return;
            this.supplementaryServiceChanged((object)this, new PropertyChangedEventArgs<SupplementaryServiceVM>(refOldValue, refNewValue));
        }
        protected void RaiseNewsAndPromotionChanged(NewsAndPromotionsVM refOldValue, NewsAndPromotionsVM refNewValue)
        {
            if (this.newsAndPromotioneChanged == null)
                return;
            this.newsAndPromotioneChanged((object)this, new PropertyChangedEventArgs<NewsAndPromotionsVM>(refOldValue, refNewValue));
        }
        protected void RaiseAlhawaNewsChanged(AlhawaVM refOldValue, AlhawaVM refNewValue)
        {
            if (this.alhawaNewsChanged == null)
                return;
            this.alhawaNewsChanged((object)this, new PropertyChangedEventArgs<AlhawaVM>(refOldValue, refNewValue));
        }
        
        protected void RaiseHashCodeChanged(string refOldValue, string refNewValue)
        {
            if (this.hashCodeChanged == null)
                return;
            this.hashCodeChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseErrorFound(string ErrorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(ErrorName));
        }

        protected void RaiseProgress(string Where)
        {
            if (this.progress == null)
                return;
            this.progress((object)this, new GenericEventArgs<string>(Where));
        }

        protected virtual void HandleBillChanged(BillVM refOldValue, BillVM refNewValue)
        {
        }

        protected virtual bool ValidateBillChange(BillVM refOldValue, BillVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleBalanceChanged(BalanceVM refOldValue, BalanceVM refNewValue)
        {
        }

        protected virtual bool ValidateBalanceChange(BalanceVM refOldValue, BalanceVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
        protected virtual void HandleNeqatyChanged(NeqatyVM refOldValue, NeqatyVM refNewValue)
        {
        }

        protected virtual bool ValidateNeqatyChange(NeqatyVM refOldValue, NeqatyVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleSupplementaryServiceChanged(SupplementaryServiceVM refOldValue, SupplementaryServiceVM refNewValue)
        {
        }

        protected virtual bool ValidateSupplementaryServiceChange(SupplementaryServiceVM refOldValue, SupplementaryServiceVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleNewsAndPromotionChanged(NewsAndPromotionsVM refOldValue, NewsAndPromotionsVM refNewValue)
        {
        }

        protected virtual bool ValidateNewsAndPromotionChange(NewsAndPromotionsVM refOldValue, NewsAndPromotionsVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
        protected virtual void HandleAlhawaNewsChanged(AlhawaVM refOldValue, AlhawaVM refNewValue)
        {
        }

        protected virtual bool ValidateAlhawaNewsChange(AlhawaVM refOldValue, AlhawaVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleHashCodeChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateHashCodeChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
    }
}