﻿using System;
using System.Globalization;
using System.Xml.Linq; 
using MobilyAppWin8.Requests;
using MobilyWin8.Requests;

namespace MobilyWin8.ViewModels
{
    public class BillVM : AbstractBillVM
    {
        public async void PostBill(string hashcode)
        {
            string msg1 =
                await
                BalanceBillRequest.LoginToEPortal(BalanceBillRequest.CreateXmlForBalanceBillNeqaty(hashcode,
                                                                                                   Constants.
                                                                                                       balancebill_GET_BILL_INFO));
            string error1 = ConstantsRequest.GetError(msg1);
            if (error1 == null || error1 == string.Empty)
            {
                this.RaiseProgress(Constants.progress_BillInfo);
                this.ParseXmlBillInfo(msg1);


                string msg2 =
                    await
                    BalanceBillRequest.LoginToEPortal(BalanceBillRequest.CreateXmlForBalanceBillNeqaty(hashcode,
                                                                                                       Constants.
                                                                                                           balancebill_GET_BILL_SUMMARY));
                string error2 = ConstantsRequest.GetError(msg2);
                if (error2 == null || error2 == string.Empty)
                {
                    this.RaiseProgress(Constants.progress_BillSummary);
                    this.ParseXmlBillSummary(msg2);
                    string msg3 =
                        await
                        BalanceBillRequest.LoginToEPortal(BalanceBillRequest.CreateXmlForBalanceBillNeqaty(hashcode,
                                                                                                           Constants.
                                                                                                               balancebill_GET_BILL_PERSONAL_INFO));
                    string error3 = ConstantsRequest.GetError(msg3);
                    if (error3 == null || error3 == string.Empty)
                    {
                        this.RaiseProgress(Constants.progress_BillPersonalInfo);
                        this.ParseXmlBillPersonalInfo(msg3);
                    }
                    else
                        this.RaiseErrorFound("Bill Personal Info :" + error3);
                }
                else
                    this.RaiseErrorFound("Bill Summary :" + error2);
            }
            else
                this.RaiseErrorFound("Bill Info :" + error1);
        }

        private void ParseXmlBillPersonalInfo(string msg)
        { 
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName) Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName) Constants.balancebill_BILL_PERSONAL_INFO)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName) Constants.balancebill_Name)
                                    this.Name = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_BillNumber)
                                    this.BillNumber = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_POBox)
                                    this.POBox = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_Address)
                                    this.Address = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_City)
                                    this.City = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_Country)
                                    this.Country = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }

        private void ParseXmlBillInfo(string msg)
        { 
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName) Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName) Constants.balancebill_BILL_INFO)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName) Constants.balancebill_StartDate)
                                    this.StartDate = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_EndDate)
                                    this.EndDate = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_Tarrif_Plan)
                                    this.TarrifPlan = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_LINENUMBER)
                                    this.LineNumber = xelement3.Value;
                                else if (xelement3.Name == (XName) Constants.balancebill_Due_Date)
                                    this.DueDate = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }

        private void ParseXmlBillSummary(string msg)
        { 
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName) Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName) Constants.balancebill_BILL_SUMMARY)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                CultureInfo invariantCulture = CultureInfo.InvariantCulture;
                                if (xelement3.Name == (XName) Constants.balancebill_Previous_Amount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.PreviousAmount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_MonthlyFee)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.MonthlyFee = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_AdditionalFee)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.AdditionalFee = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_UsageAmount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        //TODO: parse signed value
                                        this.UsageAmount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_Discount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        //TODO: parse signed value
                                        this.Discount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_PaidAmount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        //TODO: parse signed value
                                        this.PaidAmount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_AmountDue &&
                                         (xelement3.Value != null && xelement3.Value != ""))
                                    this.AmountDue = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}