﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using MobilyWin8.Common;
using MobilyWin8.Helpers;

namespace MobilyWin8.ViewModels
{
    public class AbstractSupplementaryServiceVM : BindableBase
    {

        protected ObservableCollection<SupplementaryService> m_refServicesColl = (ObservableCollection<SupplementaryService>)null;

        private PropertyChangedEventHandler<ObservableCollection<SupplementaryService>> collServicesChanged;


        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;

        public virtual ObservableCollection<SupplementaryService> ServicesColl
        {
            get
            {
                return this.m_refServicesColl;
            }
            set
            {
                ObservableCollection<SupplementaryService> refOldValue = this.m_refServicesColl;
                if (!this.ValidateServicesCollChange(refOldValue, value))
                    return;
                this.SetProperty(ref m_refServicesColl, value);
                this.HandleServicesCollChanged(refOldValue, this.m_refServicesColl);
                this.RaiseCollServicesChanged(refOldValue, this.m_refServicesColl);
            }
        }

        public event PropertyChangedEventHandler<ObservableCollection<SupplementaryService>>  CollServicesChanged
        {
            add
            {
                PropertyChangedEventHandler<ObservableCollection<SupplementaryService>> changedEventHandler = this.collServicesChanged;
                PropertyChangedEventHandler<ObservableCollection<SupplementaryService>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<SupplementaryService>>>(ref this.collServicesChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<ObservableCollection<SupplementaryService>> changedEventHandler = this.collServicesChanged;
                PropertyChangedEventHandler<ObservableCollection<SupplementaryService>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<SupplementaryService>>>(ref this.collServicesChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected virtual void HandleServicesCollChanged(ObservableCollection<SupplementaryService> refOldValue, ObservableCollection<SupplementaryService> refNewValue)
        {
        }

        protected virtual bool ValidateServicesCollChange(ObservableCollection<SupplementaryService> refOldValue, ObservableCollection<SupplementaryService> refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }


        protected void RaiseCollServicesChanged(ObservableCollection<SupplementaryService> refOldValue, ObservableCollection<SupplementaryService> refNewValue)
        {
            if (this.collServicesChanged == null)
                return;
            this.collServicesChanged((object)this, new PropertyChangedEventArgs<ObservableCollection<SupplementaryService>>(refOldValue, refNewValue));
        }
        protected void RaiseErrorFound(string errorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(errorName));
        }
        protected void RaiseProgress(string Where)
        {
            if (this.progress == null)
                return;
            this.progress((object)this, new GenericEventArgs<string>(Where));
        }
    }

    public class SupplementaryService
    {
        public string ServiceName { get; set; }
        public string Status { get; set; }
        public string SubscriptionFee { get; set; }
        public string MonthlyFee { get; set; }
    }
}