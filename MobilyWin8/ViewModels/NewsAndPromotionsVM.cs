﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Xml.Linq;
using MobilyAppWin8.Requests;
using MobilyWin8.Common;
using MobilyWin8.Helpers;
using MobilyWin8.Models;
using MobilyWin8.Requests;

namespace MobilyWin8.ViewModels
{
    public class NewsAndPromotionsVM : AbstractNewsAndPromotionsVM
    {
        public NewsAndPromotionsVM()
        {
            NewsColl = new ObservableCollection<NewsPromotionItem>();
            PromotionColl = new ObservableCollection<NewsPromotionItem>();


            
        }

        public async void PostNewsPromotions(string hashcode)
        {
            string msg1 =
               await NewsPromotionRequest.LoginToEPortal(NewsPromotionRequest.CreateXmlForNewsPromotion(hashcode,
                                                                                                  Constants.GET_LATEST_NEWS_PROMOTION, "1"));

            string error1 = ConstantsRequest.GetError(msg1);
            if (error1 == null || error1 == string.Empty)
            {
                this.RaiseProgress(Constants.progress_BillInfo);
                this.ParseXmlNews(msg1);
            }

            string msg2 =
               await NewsPromotionRequest.LoginToEPortal(NewsPromotionRequest.CreateXmlForNewsPromotion(hashcode,
                                                                                                  Constants.GET_LATEST_NEWS_PROMOTION, "2"));

            string error2 = ConstantsRequest.GetError(msg2);
            if (error2 == null || error2 == string.Empty)
            {
                this.RaiseProgress(Constants.progress_BillInfo);
                this.ParseXmlPromotions(msg2);
            }
        }



        public void ParseXmlNews(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.NewsPromotion_ItemList)
                        {
                            NewsColl = new ObservableCollection<NewsPromotionItem>();

                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == Constants.NewsPromotion_Item)
                                {
                                    NewsPromotionItem item = new NewsPromotionItem();
                                    foreach (XElement xelement4 in xelement3.Descendants())
                                    {
                                        if (xelement4.Name == (XName)Constants.NewsPromotion_ID)
                                            item.Id = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescEn)
                                            item.ShortDescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescAr)
                                            item.ShortDescAr = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescEn)
                                            item.DescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescAr)
                                            item.DescAr = xelement4.Value;
                                       // else if (xelement4.Name == (XName)Constants.NewsPromotion_Image)
                                           // item.Image = ImageDecodingHelper.ImageFromBase64String(xelement4.Value);
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ImageFormat)
                                            item.ImageFormat = xelement4.Value;
                                    }
                                    NewsColl.Add(item);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void ParseXmlPromotions(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.NewsPromotion_ItemList)
                        {
                            PromotionColl = new ObservableCollection<NewsPromotionItem>();

                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == Constants.NewsPromotion_Item)
                                {
                                    NewsPromotionItem item = new NewsPromotionItem();
                                    foreach (XElement xelement4 in xelement3.Descendants())
                                    {
                                        if (xelement4.Name == (XName)Constants.NewsPromotion_ID)
                                            item.Id = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescEn)
                                            item.ShortDescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescAr)
                                            item.ShortDescAr = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescEn)
                                            item.DescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescAr)
                                            item.DescAr = xelement4.Value;
                                        //else if (xelement4.Name == (XName)Constants.NewsPromotion_Image)
                                        //    item.Image = ImageDecodingHelper.ImageFromBase64String(xelement4.Value);
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ImageFormat)
                                            item.ImageFormat = xelement4.Value;
                                    }
                                    PromotionColl.Add(item);
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}