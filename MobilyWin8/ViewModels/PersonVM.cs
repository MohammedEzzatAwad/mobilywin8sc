﻿using System;
using MobilyWin8.Helpers;
using MobilyWin8.Requests;

namespace MobilyWin8.ViewModels
{
    public class PersonVM : AbstractPersonVM
    {
        public PersonVM(string hashCode)
        {
            if (hashCode == null && !(hashCode != string.Empty))
                return;
            this.HashCode = hashCode;
        }

        public void PostPerson()
        {
            if (HashCode == null && HashCode == string.Empty)
                return;

            Balance = new BalanceVM();
            Balance.PostBalance(this.HashCode);

            Bill = new BillVM();
            Bill.PostBill(this.HashCode);


            Neqaty = new NeqatyVM();
            Neqaty.PostNeqaty(this.HashCode);

            //SupplementaryService = new SupplementaryServiceVM();
            //SupplementaryService.PostSupplementaryService(this.HashCode);

            NewsAndPromotions = new NewsAndPromotionsVM();
            NewsAndPromotions.PostNewsPromotions(this.HashCode);


            //AlhwaNews = new AlhawaVM();
            //AlhwaNews.PostAlhawa(this.HashCode);

        }

        private void Progress(object sender, GenericEventArgs<string> e)
        {
            this.RaiseProgress(e.Param);
        }

        private void ErrorFound(object sender, GenericEventArgs<string> e)
        {
            this.RaiseErrorFound(e.Param);
        }
        

       
    }
}