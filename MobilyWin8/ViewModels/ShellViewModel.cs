﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MobilyAppWin8.Requests;
using MobilyWin8.Common;
using MobilyWin8.Helpers;
using MobilyWin8.Messaging;
using MobilyWin8.Requests;
using MobilyWin8.Services;
using MobilyWin8.Views;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;

namespace MobilyWin8.ViewModels
{
    public class ShellViewModel : BindableBase, IStatusService
    {
        private readonly ApplicationSettings _settings;
        private readonly INavigationService _navigationService;
        private readonly IHub _hub;
        private readonly ResourceLoader _resourceLoader = new ResourceLoader();

        public ShellViewModel(ApplicationSettings settings, INavigationService navigationService, IHub hub)
        {

            _settings = settings;
            _navigationService = navigationService;
            _hub = hub;

            Message = "Initializing...";
            if (PersonVM == null)
                WelcomeMessage = "Welcome, Guest";
            else
                WelcomeMessage = "Welcome, " + PersonVM.Bill.Name;

            IsLoading = true;

            GoBackCommand = _navigationService.GoBackCommand;
        }
        public ICommand GoBackCommand { get; set; }


        public bool CanGoBack
        {
            get { return _navigationService.CanGoBack; }
        }

        private string _welcomeMessage;
        public string WelcomeMessage
        {
            get { return _welcomeMessage; }
            set
            {
                SetProperty(ref _welcomeMessage, value);
            }
        }

        private string _pageTitle;
        public string PageTitle
        {
            get { return _pageTitle; }
            set
            {
                SetProperty(ref _pageTitle, value);
            }
        }

        private bool _isLoggedIn;
        public bool IsLoggedIn
        {
            get { return _isLoggedIn; }
            set { SetProperty(ref _isLoggedIn, value); }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                SetProperty(ref _isLoading, value);
            }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                if (value != null)
                {
                    CommandText = null;
                    ErrorMessage = null;
                }
                SetProperty(ref _message, value);
            }
        }

        private string _temporaryMessage;
        public string TemporaryMessage
        {
            get { return _temporaryMessage; }
            set
            {
                if (value != null)
                {
                    // always reset the message first.
                    TemporaryMessage = null;
                }
                SetProperty(ref _temporaryMessage, value);
            }
        }

        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                SetProperty(ref _errorMessage, value);
            }
        }

        private string _commandText;
        public string CommandText
        {
            get { return _commandText; }
            set
            {
                if (value != null)
                {
                    Message = null;
                }
                SetProperty(ref _commandText, value);
            }
        }

        private PersonVM _personVM;
        public PersonVM PersonVM
        {
            get { return _settings.SelectedInstance; }
            set
            {
                if (value != null)
                {
                    _settings.SelectedInstance = value;
                    SetProperty(ref _personVM, value);
                    _navigationService.Navigate(typeof(MainPage));
                }
            }
        }

        private ICommand _statusCommand;
        public ICommand StatusCommand
        {
            get { return _statusCommand; }
            set
            {
                SetProperty(ref _statusCommand, value);
            }
        }

        public async void LogIn(string userName, string password)
        {
            if (InternetChecker.IsConnectedToInternet())
            {

                if (_personVM == null)
                {


                    Message = "Is Logging In ...";
                    IsLoading = true;


                    string xmlForLogin = LoginRequest.CreateXmlForLogin(userName, password);
                    string msg = await LoginRequest.LoginToEPortal(xmlForLogin);
                    string error1 = ConstantsRequest.GetError(msg);
                    if (error1 == null || error1 == string.Empty)
                    {

                        PersonVM = LoginRequest.GetPersonVM(msg);

                        PersonVM.PostPerson();
                        if (PersonVM != null)
                        {

                            WelcomeMessage = "Welcome, " + PersonVM.Bill.Name;
                            LoginRequest.CreateXmlForLogout(PersonVM.HashCode);
                            LoginRequest.LoginToEPortal(xmlForLogin);

                            string error2 = ConstantsRequest.GetError(msg);
                            if (error2 == null || error2 == string.Empty)
                                ErrorMessage = "Logout : " + error1;

                            IsLoggedIn = true;
                        }
                    }

                    IsLoading = false;
                    ErrorMessage = "Login : " + error1;

                }
            }
            else
            {
                var md = new MessageDialog("No Internet Connection. Please connect to Internet and try again.");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();
            }
        }

        private void personVM_Progress(object sender, GenericEventArgs<string> e)
        {
            string str = e.Param;
            this.Message = str;
        }

        private void personVM_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            this.Message = e.Param;
        }

        public void SetNetworkUnavailable()
        {
            CommandText = "Settings";
            StatusCommand = new DelegateCommand(ShowSettings);
            ErrorMessage = _resourceLoader.GetString("Exception_NetworkNotAvailable");
        }

        public void SetMobilyUnavailable()
        {
            CommandText = "Settings";
            StatusCommand = new DelegateCommand(ShowSettings);
            ErrorMessage = _resourceLoader.GetString("Exception_MobilyUnavailable");
        }

        private void ShowSettings()
        {
            _hub.Send(new ShowSettingsMessage());
        }

    }
}
