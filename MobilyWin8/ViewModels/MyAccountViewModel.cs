﻿using System.Threading.Tasks;
using System.Windows.Input;
using MobilyWin8.Common;
using MobilyWin8.Messaging;
using MobilyWin8.Requests;
using MobilyWin8.Services;
using Windows.UI.Popups;

namespace MobilyWin8.ViewModels
{
    public class MyAccountViewModel : BindableBase
    {
        private readonly ApplicationSettings _settings;
        private readonly INavigationService _navigationService;
        private readonly IHub _messageHub;
        private readonly IStatusService _statusService;

        public MyAccountViewModel(ApplicationSettings settings, INavigationService navigationService, IHub messageHub, IStatusService statusService)
        {
            _settings = settings;
            _navigationService = navigationService;
            _messageHub = messageHub;
            _statusService = statusService;


            _statusService.PageTitle = "My Account";

            CreditTransferCommand = new AsyncDelegateCommand(TransferCredit);
        }

        public ICommand CreditTransferCommand { get; set; }

        private PersonVM _personVM;
        public PersonVM PersonVM
        {
            get { return _settings.SelectedInstance; }
            set
            {
                SetProperty(ref _personVM, value);
            }
        }

        private string recipientMSISDN;
        public string RecipientMSISDN
        {
            get { return recipientMSISDN; }
            set { SetProperty(ref recipientMSISDN, value); }
        }

        private string transferredAmount;
        public string TransferredAmount
        {
            get { return transferredAmount; }
            set { SetProperty(ref transferredAmount, value); }
        }

        public async Task TransferCredit()
        {
            string response = await LoginRequest.LoginToEPortal(LoginRequest.CreateXmlForCreditTransfer(PersonVM.HashCode, "", ""));
            bool isConfiremed = LoginRequest.CreditTransferConfirmation(response);
            if (isConfiremed)
            {
                var md = new MessageDialog(transferredAmount + " is succssfully transfered to " + RecipientMSISDN + ".");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();
            }
            else if(string.IsNullOrEmpty(transferredAmount) || string.IsNullOrEmpty(recipientMSISDN))
            {
                var md = new MessageDialog("Please fill Recipient's Number.");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();
            }
            else
            {
                var md = new MessageDialog("Credit Transfer is failed.");
                md.Commands.Add(new UICommand("Ok"));
                md.ShowAsync();

            }
        }

        
    }
}