﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Xml.Linq;
using MobilyAppWin8.Requests;
using MobilyWin8.Requests;

namespace MobilyWin8.ViewModels
{
    public class SupplementaryServiceVM : AbstractSupplementaryServiceVM
    {
        public async void PostSupplementaryService(string hashcode)
        {
            ServicesColl = new ObservableCollection<SupplementaryService>();
            string requestMessage = BalanceBillRequest.CreateXmlForBalanceBillNeqaty(hashcode,
                                                                                     Constants.
                                                                                         SUPPLEMENTARY_SERVICES_INQUIRY);

            string msg = await BalanceBillRequest.LoginToEPortal(requestMessage);
            msg = msg.Replace("---!>", "-->");
            string error = ConstantsRequest.GetError(msg);
            if (error == null || error == string.Empty)
                this.ParseXml(msg);
            else
                this.RaiseErrorFound("Supplementary Services :" + error);
        }

        private void ParseXml(string msg)
        {
                       
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName) Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName) Constants.SUPPLEMENTARY_SERVICES_INQUIRY)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if(xelement3.Name == (XName)Constants.ListOfSupplementaryService)
                                {
                                    foreach (var xelement4 in xelement3.Descendants())
                                    {
                                        if(xelement4.Name == (XName)Constants.SupplementaryService)
                                        {
                                            SupplementaryService service = new SupplementaryService();

                                            foreach (var xelement5 in xelement4.Descendants())
                                            {
                                                if(xelement5.Name == Constants.SupplementaryService_ServiceName)
                                                {
                                                    service.ServiceName = xelement5.Value;
                                                }
                                                else if(xelement5.Name == Constants.SupplementaryService_Status)
                                                {
                                                    service.Status = xelement5.Value;
                                                }
                                                else if(xelement5.Name == Constants.SupplementaryService_SubscriptionFee)
                                                {
                                                    service.SubscriptionFee = xelement5.Value;
                                                }
                                                else if(xelement5.Name == Constants.SupplementaryService_MonthlyFee)
                                                {
                                                    service.MonthlyFee = xelement5.Value;
                                                }
                                                ServicesColl.Add(service);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


    }
}