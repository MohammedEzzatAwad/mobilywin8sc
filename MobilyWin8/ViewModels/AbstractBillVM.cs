﻿using System;
using System.Threading;
using MobilyWin8.Common;
using MobilyWin8.Helpers;

namespace MobilyWin8.ViewModels
{
    public class AbstractBillVM : BindableBase
    {
        protected string m_refName = "";
        protected string m_refBillNumber = "";
        protected string m_refPOBox = "";
        protected string m_refAddress = "";
        protected string m_refCity = "";
        protected string m_refCountry = "";
        protected string m_refStartDate = "";
        protected string m_refEndDate = "";
        protected string m_refTarrifPlan = "";
        protected string m_refLineNumber = "";
        protected string m_refDueDate = "";
        protected string m_refPreviousAmount = "";
        protected string m_refMonthlyFee = "";
        protected string m_refAdditionalFee = "";
        protected string m_refUsageAmount = "";
        protected string m_refDiscount = "";
        protected string m_refPaidAmount = "";
        protected string m_refAmountDue = "";
        private PropertyChangedEventHandler<string> nameChanged;
        private PropertyChangedEventHandler<string> billNumberChanged;
        private PropertyChangedEventHandler<string> pOBoxChanged;
        private PropertyChangedEventHandler<string> addressChanged;
        private PropertyChangedEventHandler<string> cityChanged;
        private PropertyChangedEventHandler<string> countryChanged;
        private PropertyChangedEventHandler<string> startDateChanged;
        private PropertyChangedEventHandler<string> endDateChanged;
        private PropertyChangedEventHandler<string> tarrifPlanChanged;
        private PropertyChangedEventHandler<string> lineNumberChanged;
        private PropertyChangedEventHandler<string> dueDateChanged;
        private PropertyChangedEventHandler<string> previousAmountChanged;
        private PropertyChangedEventHandler<string> monthlyFeeChanged;
        private PropertyChangedEventHandler<string> additionalFeeChanged;
        private PropertyChangedEventHandler<string> usageAmountChanged;
        private PropertyChangedEventHandler<string> discountChanged;
        private PropertyChangedEventHandler<string> paidAmountChanged;
        private PropertyChangedEventHandler<string> amountDueChanged;
        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;

       

        public virtual string Name
        {
            get
            {
                return this.m_refName;
            }
            set
            {
                string refOldValue = this.m_refName;
                if (!this.ValidateNameChange(refOldValue, value))
                    return;
                SetProperty(ref m_refName , value);
                this.HandleNameChanged(refOldValue, this.m_refName);
                
                this.RaiseNameChanged(refOldValue, this.m_refName);
            }
        }

        public virtual string BillNumber
        {
            get
            {
                return this.m_refBillNumber;
            }
            set
            {
                string refOldValue = this.m_refBillNumber;
                if (!this.ValidateBillNumberChange(refOldValue, value))
                    return;
                SetProperty(ref m_refBillNumber ,value);
                this.HandleBillNumberChanged(refOldValue, this.m_refBillNumber);
                
                this.RaiseBillNumberChanged(refOldValue, this.m_refBillNumber);
            }
        }

        public virtual string POBox
        {
            get
            {
                return this.m_refPOBox;
            }
            set
            {
                string refOldValue = this.m_refPOBox;
                if (!this.ValidatePOBoxChange(refOldValue, value))
                    return;
                SetProperty(ref m_refPOBox , value);
                this.HandlePOBoxChanged(refOldValue, this.m_refPOBox);
                
                this.RaisePOBoxChanged(refOldValue, this.m_refPOBox);
            }
        }

        public virtual string Address
        {
            get
            {
                return this.m_refAddress;
            }
            set
            {
                string refOldValue = this.m_refAddress;
                if (!this.ValidateAddressChange(refOldValue, value))
                    return;
                SetProperty(ref m_refAddress , value);
                this.HandleAddressChanged(refOldValue, this.m_refAddress);
                
                this.RaiseAddressChanged(refOldValue, this.m_refAddress);
            }
        }

        public virtual string City
        {
            get
            {
                return  m_refCity;
            }
            set
            {
                string refOldValue =  m_refCity;
                if (!this.ValidateCityChange(refOldValue, value))
                    return;
                SetProperty(ref m_refCity ,value);
                this.HandleCityChanged(refOldValue,  m_refCity);
             
                this.RaiseCityChanged(refOldValue,  m_refCity);
            }
        }

        public virtual string Country
        {
            get
            {
                return this.m_refCountry;
            }
            set
            {
                string refOldValue = this.m_refCountry;
                if (!this.ValidateCountryChange(refOldValue, value))
                    return;
                SetProperty(ref m_refCountry , value);
                this.HandleCountryChanged(refOldValue, this.m_refCountry);
                
                this.RaiseCountryChanged(refOldValue, this.m_refCountry);
            }
        }

        public virtual string StartDate
        {
            get
            {
                return this.m_refStartDate;
            }
            set
            {
                string refOldValue = this.m_refStartDate;
                if (!this.ValidateStartDateChange(refOldValue, value))
                    return;
               SetProperty( ref m_refStartDate ,value);
                this.HandleStartDateChanged(refOldValue, this.m_refStartDate);
               
                this.RaiseStartDateChanged(refOldValue, this.m_refStartDate);
            }
        }

        public virtual string EndDate
        {
            get
            {
                return this.m_refEndDate;
            }
            set
            {
                string refOldValue = this.m_refEndDate;
                if (!this.ValidateEndDateChange(refOldValue, value))
                    return;
                SetProperty(ref m_refEndDate , value);
                this.HandleEndDateChanged(refOldValue, this.m_refEndDate);
                 
                this.RaiseEndDateChanged(refOldValue, this.m_refEndDate);
            }
        }

        public virtual string TarrifPlan
        {
            get
            {
                return this.m_refTarrifPlan;
            }
            set
            {
                string refOldValue = this.m_refTarrifPlan;
                if (!this.ValidateTarrifPlanChange(refOldValue, value))
                    return;
                SetProperty(ref m_refTarrifPlan, value);
                this.HandleTarrifPlanChanged(refOldValue, this.m_refTarrifPlan);
                
                this.RaiseTarrifPlanChanged(refOldValue, this.m_refTarrifPlan);
            }
        }

        public virtual string LineNumber
        {
            get
            {
                return this.m_refLineNumber;
            }
            set
            {
                string refOldValue = this.m_refLineNumber;
                if (!this.ValidateLineNumberChange(refOldValue, value))
                    return;
               SetProperty(ref m_refLineNumber , value);
                this.HandleLineNumberChanged(refOldValue, this.m_refLineNumber);
                 
                this.RaiseLineNumberChanged(refOldValue, this.m_refLineNumber);
            }
        }

        public virtual string DueDate
        {
            get
            {
                return this.m_refDueDate;
            }
            set
            {
                string refOldValue = this.m_refDueDate;
                if (!this.ValidateDueDateChange(refOldValue, value))
                    return;
                SetProperty(ref m_refDueDate ,value);
                this.HandleDueDateChanged(refOldValue, this.m_refDueDate);
              
                this.RaiseDueDateChanged(refOldValue, this.m_refDueDate);
            }
        }

        public virtual string PreviousAmount
        {
            get
            {
                return this.m_refPreviousAmount;
            }
            set
            {
                string refOldValue = this.m_refPreviousAmount;
                if (!this.ValidatePreviousAmountChange(refOldValue, value))
                    return;
                SetProperty(ref m_refPreviousAmount , value);
                this.HandlePreviousAmountChanged(refOldValue, this.m_refPreviousAmount);
                
                this.RaisePreviousAmountChanged(refOldValue, this.m_refPreviousAmount);
            }
        }

        public virtual string MonthlyFee
        {
            get
            {
                return this.m_refMonthlyFee;
            }
            set
            {
                string refOldValue = this.m_refMonthlyFee;
                if (!this.ValidateMonthlyFeeChange(refOldValue, value))
                    return;
               SetProperty(ref m_refMonthlyFee ,value);
                this.HandleMonthlyFeeChanged(refOldValue, this.m_refMonthlyFee);
                 
                this.RaiseMonthlyFeeChanged(refOldValue, this.m_refMonthlyFee);
            }
        }

        public virtual string AdditionalFee
        {
            get
            {
                return this.m_refAdditionalFee;
            }
            set
            {
                string refOldValue = this.m_refAdditionalFee;
                if (!this.ValidateAdditionalFeeChange(refOldValue, value))
                    return;
               SetProperty(ref m_refAdditionalFee,value);
                this.HandleAdditionalFeeChanged(refOldValue, this.m_refAdditionalFee);
                
                this.RaiseAdditionalFeeChanged(refOldValue, this.m_refAdditionalFee);
            }
        }

        public virtual string UsageAmount
        {
            get
            {
                return this.m_refUsageAmount;
            }
            set
            {
                string refOldValue = this.m_refUsageAmount;
                if (!this.ValidateUsageAmountChange(refOldValue, value))
                    return;
                SetProperty(ref m_refUsageAmount , value);
                this.HandleUsageAmountChanged(refOldValue, this.m_refUsageAmount);
              
                this.RaiseUsageAmountChanged(refOldValue, this.m_refUsageAmount);
            }
        }

        public virtual string Discount
        {
            get
            {
                return this.m_refDiscount;
            }
            set
            {
                string refOldValue = this.m_refDiscount;
                if (!this.ValidateDiscountChange(refOldValue, value))
                    return;
                SetProperty(ref m_refDiscount ,value);
                this.HandleDiscountChanged(refOldValue, this.m_refDiscount);
                 
                this.RaiseDiscountChanged(refOldValue, this.m_refDiscount);
            }
        }

        public virtual string PaidAmount
        {
            get
            {
                return this.m_refPaidAmount;
            }
            set
            {
                string refOldValue = this.m_refPaidAmount;
                if (!this.ValidatePaidAmountChange(refOldValue, value))
                    return;
                SetProperty(ref m_refPaidAmount , value);
                this.HandlePaidAmountChanged(refOldValue, this.m_refPaidAmount);
               
                this.RaisePaidAmountChanged(refOldValue, this.m_refPaidAmount);
            }
        }

        public virtual string AmountDue
        {
            get
            {
                return this.m_refAmountDue;
            }
            set
            {
                string refOldValue = this.m_refAmountDue;
                if (!this.ValidateAmountDueChange(refOldValue, value))
                    return;
             SetProperty(ref m_refAmountDue , value);
                this.HandleAmountDueChanged(refOldValue, this.m_refAmountDue);
             
                this.RaiseAmountDueChanged(refOldValue, this.m_refAmountDue);
            }
        }

        public event PropertyChangedEventHandler<string> NameChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.nameChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.nameChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.nameChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.nameChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> BillNumberChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.billNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.billNumberChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.billNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.billNumberChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> POBoxChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.pOBoxChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.pOBoxChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.pOBoxChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.pOBoxChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> AddressChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.addressChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.addressChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.addressChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.addressChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> CityChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.cityChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.cityChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.cityChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.cityChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> CountryChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.countryChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.countryChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.countryChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.countryChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> StartDateChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.startDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.startDateChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.startDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.startDateChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> EndDateChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.endDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.endDateChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.endDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.endDateChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> TarrifPlanChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.tarrifPlanChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.tarrifPlanChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.tarrifPlanChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.tarrifPlanChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> LineNumberChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.lineNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.lineNumberChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.lineNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.lineNumberChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> DueDateChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.dueDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.dueDateChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.dueDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.dueDateChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> PreviousAmountChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.previousAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.previousAmountChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.previousAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.previousAmountChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> MonthlyFeeChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.monthlyFeeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.monthlyFeeChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.monthlyFeeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.monthlyFeeChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> AdditionalFeeChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.additionalFeeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.additionalFeeChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.additionalFeeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.additionalFeeChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> UsageAmountChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.usageAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.usageAmountChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.usageAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.usageAmountChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> DiscountChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.discountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.discountChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.discountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.discountChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> PaidAmountChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.paidAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.paidAmountChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.paidAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.paidAmountChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> AmountDueChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.amountDueChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.amountDueChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.amountDueChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.amountDueChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected void RaiseNameChanged(string refOldValue, string refNewValue)
        {
            if (this.nameChanged == null)
                return;
            this.nameChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseBillNumberChanged(string refOldValue, string refNewValue)
        {
            if (this.billNumberChanged == null)
                return;
            this.billNumberChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaisePOBoxChanged(string refOldValue, string refNewValue)
        {
            if (this.pOBoxChanged == null)
                return;
            this.pOBoxChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseAddressChanged(string refOldValue, string refNewValue)
        {
            if (this.addressChanged == null)
                return;
            this.addressChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseCityChanged(string refOldValue, string refNewValue)
        {
            if (this.cityChanged == null)
                return;
            this.cityChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseCountryChanged(string refOldValue, string refNewValue)
        {
            if (this.countryChanged == null)
                return;
            this.countryChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseStartDateChanged(string refOldValue, string refNewValue)
        {
            if (this.startDateChanged == null)
                return;
            this.startDateChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseEndDateChanged(string refOldValue, string refNewValue)
        {
            if (this.endDateChanged == null)
                return;
            this.endDateChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseTarrifPlanChanged(string refOldValue, string refNewValue)
        {
            if (this.tarrifPlanChanged == null)
                return;
            this.tarrifPlanChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseLineNumberChanged(string refOldValue, string refNewValue)
        {
            if (this.lineNumberChanged == null)
                return;
            this.lineNumberChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseDueDateChanged(string refOldValue, string refNewValue)
        {
            if (this.dueDateChanged == null)
                return;
            this.dueDateChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaisePreviousAmountChanged(string refOldValue, string refNewValue)
        {
            if (this.previousAmountChanged == null)
                return;
            this.previousAmountChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseMonthlyFeeChanged(string refOldValue, string refNewValue)
        {
            if (this.monthlyFeeChanged == null)
                return;
            this.monthlyFeeChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseAdditionalFeeChanged(string refOldValue, string refNewValue)
        {
            if (this.additionalFeeChanged == null)
                return;
            this.additionalFeeChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseUsageAmountChanged(string refOldValue, string refNewValue)
        {
            if (this.usageAmountChanged == null)
                return;
            this.usageAmountChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseDiscountChanged(string refOldValue, string refNewValue)
        {
            if (this.discountChanged == null)
                return;
            this.discountChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaisePaidAmountChanged(string refOldValue, string refNewValue)
        {
            if (this.paidAmountChanged == null)
                return;
            this.paidAmountChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseAmountDueChanged(string refOldValue, string refNewValue)
        {
            if (this.amountDueChanged == null)
                return;
            this.amountDueChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseErrorFound(string ErrorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(ErrorName));
        }

        protected void RaiseProgress(string Where)
        {
            if (this.progress == null)
                return;
            this.progress((object)this, new GenericEventArgs<string>(Where));
        }

        protected virtual void HandleNameChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateNameChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleBillNumberChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateBillNumberChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandlePOBoxChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidatePOBoxChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleAddressChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateAddressChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleCityChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateCityChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleCountryChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateCountryChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleStartDateChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateStartDateChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleEndDateChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateEndDateChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleTarrifPlanChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateTarrifPlanChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleLineNumberChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateLineNumberChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleDueDateChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateDueDateChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandlePreviousAmountChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidatePreviousAmountChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleMonthlyFeeChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateMonthlyFeeChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleAdditionalFeeChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateAdditionalFeeChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleUsageAmountChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateUsageAmountChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleDiscountChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateDiscountChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandlePaidAmountChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidatePaidAmountChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleAmountDueChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateAmountDueChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
    }
}