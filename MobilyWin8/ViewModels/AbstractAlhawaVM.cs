﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using MobilyWin8.Common;
using MobilyWin8.Helpers;
using MobilyWin8.Models;

namespace MobilyWin8.ViewModels
{
    public class AbstractAlhawaVM : BindableBase
    {
        //protected ObservableCollection<AlhawaNewsItem> m_refNewsFromAlhawaColl = (ObservableCollection<AlhawaNewsItem>)null;
        //protected ObservableCollection<AlhawaNewsItem> m_refSportsFromAlhawa = null;

        //private PropertyChangedEventHandler<ObservableCollection<AlhawaNewsItem>> newsFromAlhawaCollChanged;
        //private PropertyChangedEventHandler<ObservableCollection<AlhawaNewsItem>> sportsFromAlhawaCollChanged;

        //private EventHandler<GenericEventArgs<string>> errorFound;
        //private EventHandler<GenericEventArgs<string>> progress;

        //public virtual ObservableCollection<AlhawaNewsItem> NewsFromAlhawaItemColl
        //{
        //    get
        //    {
        //        return this.m_refNewsFromAlhawaColl;
        //    }
        //    set
        //    {
        //        ObservableCollection<AlhawaNewsItem> refOldValue = this.m_refNewsFromAlhawaColl;
        //        if (!this.ValidateNewsFromAlhawaCollChange(refOldValue, value))
        //            return;
        //        this.SetProperty(ref m_refNewsFromAlhawaColl, value);
        //        this.HandleNewsFromAlhawaCollChanged(refOldValue, this.m_refNewsFromAlhawaColl);
        //        this.RaiseNewsFromAlhawaCollChanged(refOldValue, this.m_refNewsFromAlhawaColl);
        //    }
        //}
        //public virtual ObservableCollection<AlhawaNewsItem> SportsFromAlhawaColl
        //{
        //    get
        //    {
        //        return this.m_refSportsFromAlhawa;
        //    }
        //    set
        //    {
        //        ObservableCollection<AlhawaNewsItem> refOldValue = this.m_refSportsFromAlhawa;
        //        if (!this.ValidateAlhawaNewsCollChange(refOldValue, value))
        //            return;
        //        this.SetProperty(ref m_refSportsFromAlhawa, value);
        //        this.HandleAlhawaNewsItemCollChanged(refOldValue, this.m_refSportsFromAlhawa);
        //        this.RaiseAlhawaNewsItemCollChanged(refOldValue, this.m_refSportsFromAlhawa);
        //    }
        //}

        //public event EventHandler<GenericEventArgs<string>> Progress
        //{
        //    add
        //    {
        //        EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
        //        EventHandler<GenericEventArgs<string>> comparand;
        //        do
        //        {
        //            comparand = eventHandler;
        //            eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
        //        }
        //        while (eventHandler != comparand);
        //    }
        //    remove
        //    {
        //        EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
        //        EventHandler<GenericEventArgs<string>> comparand;
        //        do
        //        {
        //            comparand = eventHandler;
        //            eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
        //        }
        //        while (eventHandler != comparand);
        //    }
        //}

        //public event EventHandler<GenericEventArgs<string>> ErrorFound
        //{
        //    add
        //    {
        //        EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
        //        EventHandler<GenericEventArgs<string>> comparand;
        //        do
        //        {
        //            comparand = eventHandler;
        //            eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
        //        }
        //        while (eventHandler != comparand);
        //    }
        //    remove
        //    {
        //        EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
        //        EventHandler<GenericEventArgs<string>> comparand;
        //        do
        //        {
        //            comparand = eventHandler;
        //            eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
        //        }
        //        while (eventHandler != comparand);
        //    }
        //}


        //protected virtual void HandleNewsFromAlhawaCollChanged(ObservableCollection<AlhawaNewsItem> refOldValue, ObservableCollection<AlhawaNewsItem> refNewValue)
        //{
        //}

        //protected virtual bool ValidateAlhawaNewsCollChange(ObservableCollection<AlhawaNewsItem> refOldValue, ObservableCollection<AlhawaNewsItem> refRequestedValue)
        //{
        //    return refOldValue != refRequestedValue;
        //}


        //protected void RaiseNewsFromAlhawaCollChanged(ObservableCollection<AlhawaNewsItem> refOldValue, ObservableCollection<AlhawaNewsItem> refNewValue)
        //{
        //    if (this.newsFromAlhawaCollChanged == null)
        //        return;
        //    this.newsFromAlhawaCollChanged((object)this, new PropertyChangedEventArgs<ObservableCollection<AlhawaNewsItem>>(refOldValue, refNewValue));
        //}
    }

    public class AlhawaNewsItem
    {
        public string CHANNEL_ID { get; set; }
        public string CHANNEL_NAME_EN { get; set; }
        public string CHANNEL_NAME_Ar { get; set; }
        public string CHANNEL_IMG { get; set; }
    }
}