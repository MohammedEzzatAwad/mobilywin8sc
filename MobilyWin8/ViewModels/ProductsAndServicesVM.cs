﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using MobilyWin8.Common;
using MobilyWin8.Messaging;
using MobilyWin8.Models;
using MobilyWin8.Services;
using Windows.ApplicationModel;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace MobilyWin8.ViewModels
{
    public class ProductsAndServicesVM : BindableBase
    {
        private readonly ApplicationSettings _settings;
        private readonly INavigationService _navigationService;
        private readonly IHub _messageHub;
        private readonly IStatusService _statusService;

        private  ObservableCollection<Item> itemsCollection = null; 


        private const string ItemsFileName = @"LocalData\Items.xml";

        private StorageFile _storageFile;
        private StorageFolder _storageFolder;

        public ProductsAndServicesVM(ApplicationSettings settings, INavigationService navigationService, IHub messageHub, IStatusService statusService)
        {
            _settings = settings;
            _navigationService = navigationService;
            _messageHub = messageHub;
            _statusService = statusService;

           

            loadLocalDataFromDisk();
            _statusService.PageTitle = "Products & Services";
            GoBackCommand = _navigationService.GoBackCommand;
        }

        private async void loadLocalDataFromDisk()
        {
            ItemsCollection = new ObservableCollection<Item>();
            var sf = await Package.Current.InstalledLocation.GetFileAsync(ItemsFileName);
            var file = await sf.OpenAsync(FileAccessMode.Read);
            Stream inStream = file.AsStreamForRead();
            XElement myFirstXmlElement = XDocument.Load(inStream).Elements().First();


            int posIndex = 0;
            int preIndex = 0;
            int serviceIndex = 0;
            foreach (var xmlNode in myFirstXmlElement.Descendants())
            {
                foreach (var xmlItem in xmlNode.Descendants())
                {
                    Item item = new Item();
                    if (xmlItem.Name == "Name")
                    {
                        item.Name = xmlItem.Value;
                    }
                    else if (xmlItem.Name == "Title")
                    {
                        item.Title = xmlItem.Value;
                    }
                    else if (xmlItem.Name == "Link")
                    {
                        item.Link = xmlItem.Value;
                    }
                    else if (xmlItem.Name == "Logo")
                    {
                        item.Logo = xmlItem.Value;
                    }
                    else if (xmlItem.Name == "Category")
                    {
                        item.Category = xmlItem.Value;

                    }
                    else if (xmlItem.Name == "Description")
                    {
                        item.Description = xmlItem.Value;
                    }
                    ItemsCollection.Add(item);
                }
            }

        }

        //if (xmlNode.Category.ToString() == "Prepaid")
                //{
                //    item.Index = preIndex;
                //    preIndex++;
                //}
                //else if (item.Category.ToString() == "Postpaid")
                //{
                //    item.Index = posIndex;
                //    posIndex++;
                //}
                //else if (item.Category.ToString() == "Services")
                //{
                //    item.Index = serviceIndex;
                //    serviceIndex++;
                //}
                 
         

        private List<Item> GetAllItemsFromXMl(XmlDocument doc)
        {
            List<Item> allItems = new List<Item>();
            Item item;

            foreach (var node in doc.ChildNodes)
            {
                foreach (var chnde in node.ChildNodes)
                {
                    if (chnde.NodeName == "Item")
                    {
                        item = new Item();
                        foreach (var subch in chnde.ChildNodes)
                        {
                            if (subch.ChildNodes.Count > 0)
                            {
                                foreach (var subsub in subch.ChildNodes)
                                {
                                    if (subch.NodeName.ToString() == "Name")
                                    {
                                        item.Name = subsub.NodeValue.ToString();
                                    }
                                    else if (subch.NodeName.ToString() == "Title")
                                    {
                                        item.Title = subsub.NodeValue.ToString();
                                    }
                                    else if (subch.NodeName.ToString() == "Logo")
                                    {
                                        item.Logo = subsub.NodeValue.ToString();
                                    }
                                    else if (subch.NodeName.ToString() == "Category")
                                    {
                                        item.Category = subsub.NodeValue.ToString();
                                    }
                                    else if (subch.NodeName.ToString() == "Description")
                                    {
                                        item.Description = subsub.NodeValue.ToString();
                                    }

                                }
                            }

                        }
                        allItems.Add(item);
                    }
                }
            }
            return allItems;

        }

        public ICommand GoBackCommand { get; set; }


       

        //required for groupping included index for each item 
        public ObservableCollection<Item> ItemsCollection
        {
            get { return itemsCollection; }
            set
            {
                if (value == null)
                {

                }
                SetProperty(ref itemsCollection, value);

            }

        }

        //Not working 
        public async Task<XmlDocument> ReadItemsFromDisk()
        {
            try
            {
                _storageFolder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Models");
                _storageFile = await _storageFolder.GetFileAsync(ItemsFileName);

                var loadSettings = new XmlLoadSettings { ProhibitDtd = false, ResolveExternals = false };
                return await XmlDocument.LoadFromFileAsync(_storageFile, loadSettings);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}