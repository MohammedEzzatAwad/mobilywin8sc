﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using MobilyWin8.Common;
using MobilyWin8.Helpers;

namespace MobilyWin8.ViewModels
{
    public abstract class AbstractApplicationVM : BindableBase
    {
        protected ObservableCollection<PersonVM> m_refCollPerson = (ObservableCollection<PersonVM>)null;
        protected string m_refLogin = "";
        protected string m_refPassword = "";
        protected string m_refErrorText = "";
        protected string m_refProgressText = "";
        protected DelegateCommand<object> m_cmdValidate = (DelegateCommand<object>)null;
        private PropertyChangedEventHandler<ObservableCollection<PersonVM>> collPersonChanged;
        private PropertyChangedEventHandler<string> loginChanged;
        private PropertyChangedEventHandler<string> passwordChanged;
        private PropertyChangedEventHandler<string> errorTextChanged;
        private PropertyChangedEventHandler<string> progressTextChanged;

        public AbstractApplicationVM()
        {
            //this.m_cmdValidate = new DelegateCommand(this.ExecuteValidate,this.CanExecuteValidate);
        }

        public virtual ObservableCollection<PersonVM> CollPerson
        {
            get
            {
                return this.m_refCollPerson;
            }
            set
            {
                ObservableCollection<PersonVM> refOldValue = this.m_refCollPerson;
                if (!this.ValidateCollPersonChange(refOldValue, value))
                    return;
                this.SetProperty(ref m_refCollPerson, value);
                this.HandleCollPersonChanged(refOldValue, this.m_refCollPerson);
                this.RaiseCollPersonChanged(refOldValue, this.m_refCollPerson);
            }
        }

        public virtual string Login
        {
            get
            {
                return this.m_refLogin;
            }
            set
            {
                string refOldValue = this.m_refLogin;
                if (!this.ValidateLoginChange(refOldValue, value))
                    return;
                SetProperty(ref m_refLogin, value);
                this.HandleLoginChanged(refOldValue, this.m_refLogin);
                this.RaiseLoginChanged(refOldValue, this.m_refLogin);
            }
        }

        public virtual string Password
        {
            get
            {
                return this.m_refPassword;
            }
            set
            {
                string refOldValue = this.m_refPassword;
                if (!this.ValidatePasswordChange(refOldValue, value))
                    return;
                SetProperty(ref m_refPassword, value);
                this.HandlePasswordChanged(refOldValue, this.m_refPassword);

                this.RaisePasswordChanged(refOldValue, this.m_refPassword);
            }
        }

        public virtual string ErrorText
        {
            get
            {
                return this.m_refErrorText;
            }
            set
            {
                string refOldValue = this.m_refErrorText;
                if (!this.ValidateErrorTextChange(refOldValue, value))
                    return;
                SetProperty(ref m_refErrorText, value);
                this.HandleErrorTextChanged(refOldValue, this.m_refErrorText);
                this.RaiseErrorTextChanged(refOldValue, this.m_refErrorText);
            }
        }

        public virtual string ProgressText
        {
            get
            {
                return this.m_refProgressText;
            }
            set
            {
                string refOldValue = this.m_refProgressText;
                if (!this.ValidateProgressTextChange(refOldValue, value))
                    return;
                SetProperty(ref  m_refProgressText, value);
                this.HandleProgressTextChanged(refOldValue, this.m_refProgressText);
                this.RaiseProgressTextChanged(refOldValue, this.m_refProgressText);
            }
        }

        public virtual DelegateCommand<object> Validate
        {
            get
            {
                return this.m_cmdValidate;
            }
        }

        public event PropertyChangedEventHandler<ObservableCollection<PersonVM>> CollPersonChanged
        {
            add
            {
                PropertyChangedEventHandler<ObservableCollection<PersonVM>> changedEventHandler = this.collPersonChanged;
                PropertyChangedEventHandler<ObservableCollection<PersonVM>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<PersonVM>>>(ref this.collPersonChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<ObservableCollection<PersonVM>> changedEventHandler = this.collPersonChanged;
                PropertyChangedEventHandler<ObservableCollection<PersonVM>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<PersonVM>>>(ref this.collPersonChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> LoginChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.loginChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.loginChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.loginChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.loginChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> PasswordChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.passwordChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.passwordChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.passwordChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.passwordChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> ErrorTextChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.errorTextChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.errorTextChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.errorTextChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.errorTextChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> ProgressTextChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.progressTextChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.progressTextChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.progressTextChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.progressTextChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }



        protected void RaiseCollPersonChanged(ObservableCollection<PersonVM> refOldValue, ObservableCollection<PersonVM> refNewValue)
        {
            if (this.collPersonChanged == null)
                return;
            this.collPersonChanged((object)this, new PropertyChangedEventArgs<ObservableCollection<PersonVM>>(refOldValue, refNewValue));
        }

        protected void RaiseLoginChanged(string refOldValue, string refNewValue)
        {
            if (this.loginChanged == null)
                return;
            this.loginChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaisePasswordChanged(string refOldValue, string refNewValue)
        {
            if (this.passwordChanged == null)
                return;
            this.passwordChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseErrorTextChanged(string refOldValue, string refNewValue)
        {
            if (this.errorTextChanged == null)
                return;
            this.errorTextChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseProgressTextChanged(string refOldValue, string refNewValue)
        {
            if (this.progressTextChanged == null)
                return;
            this.progressTextChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected virtual void HandleCollPersonChanged(ObservableCollection<PersonVM> refOldValue, ObservableCollection<PersonVM> refNewValue)
        {
        }

        protected virtual bool ValidateCollPersonChange(ObservableCollection<PersonVM> refOldValue, ObservableCollection<PersonVM> refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleLoginChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateLoginChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandlePasswordChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidatePasswordChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleErrorTextChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateErrorTextChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleProgressTextChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateProgressTextChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void ExecuteValidate()
        {
        }

        //protected virtual void ExecuteValidate(object objArguments)
        //{
        //}
        protected virtual bool CanExecuteValidate()
        {
            return true;
        }
        //protected virtual bool CanExecuteValidate(object objArguments)
        //{
        //    return true;
        //}
    }
}