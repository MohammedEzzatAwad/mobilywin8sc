﻿using System;
using System.Collections.ObjectModel;
using MobilyWin8.Common;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace MobilyWin8.LocalData
{
       /// <summary>
    /// Base class for <see cref="SampleDataItem"/> and <see cref="SampleDataGroup"/> that
    /// defines properties common to both.
    /// </summary>
    [Windows.Foundation.Metadata.WebHostHidden]
    public abstract class SampleDataCommon : BindableBase
    {
        private static Uri _baseUri = new Uri("ms-appx:///");

        public SampleDataCommon(String uniqueId, String title, String subtitle, String imagePath, String description)
        {
            this._uniqueId = uniqueId;
            this._title = title;
            this._subtitle = subtitle;
            this._description = description;
            this._imagePath = imagePath;
        }
        
        private string _uniqueId = string.Empty;
        public string UniqueId
        {
            get { return this._uniqueId; }
            set { this.SetProperty(ref this._uniqueId, value); }
        }

        private string _title = string.Empty;
        public string Title
        {
            get { return this._title; }
            set { this.SetProperty(ref this._title, value); }
        }

        private string _subtitle = string.Empty;
        public string Subtitle
        {
            get { return this._subtitle; }
            set { this.SetProperty(ref this._subtitle, value); }
        }

        private string _description = string.Empty;
        public string Description
        {
            get { return this._description; }
            set { this.SetProperty(ref this._description, value); }
        }

        private ImageSource _image = null;
        private String _imagePath = null;
        public ImageSource Image
        {
            get
            {
                if (this._image == null && this._imagePath != null)
                {
                    this._image = new BitmapImage(new Uri(SampleDataCommon._baseUri, this._imagePath));
                }
                return this._image;
            }

            set
            {
                this._imagePath = null;
                this.SetProperty(ref this._image, value);
            }
        }

        public void SetImage(String path)
        {
            this._image = null;
            this._imagePath = path;
            this.OnPropertyChanged("Image");
        }
    }

    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class SampleDataItem : SampleDataCommon
    {
        public SampleDataItem(String uniqueId, String title, String subtitle, String imagePath, String description, String content, SampleDataGroup group)
            : base(uniqueId, title, subtitle, imagePath, description)
        {
            this._content = content;
            this._group = group;
        }

        private string _content = string.Empty;
        public string Content
        {
            get { return this._content; }
            set { this.SetProperty(ref this._content, value); }
        }

        private SampleDataGroup _group;
        public SampleDataGroup Group
        {
            get { return this._group; }
            set { this.SetProperty(ref this._group, value); }
        }
    }

    /// <summary>
    /// Generic group data model.
    /// </summary>
    public class SampleDataGroup : SampleDataCommon
    {
        public SampleDataGroup(String uniqueId, String title, String subtitle, String imagePath, String description)
            : base(uniqueId, title, subtitle, imagePath, description)
        {

        }

      

        private ObservableCollection<SampleDataItem> _items = new ObservableCollection<SampleDataItem>();
        public ObservableCollection<SampleDataItem> Items
        {
            get { return this._items; }
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with hard-coded content.
    /// </summary>
    public sealed class SampleDataSource
    {
        private ObservableCollection<SampleDataGroup> _itemGroups = new ObservableCollection<SampleDataGroup>();

        public ObservableCollection<SampleDataGroup> ItemGroups
        {
            get { return this._itemGroups; }
        }

        public SampleDataSource()
        {
            String ITEM_CONTENT = String.Format("Item Content: {0}\n\n{0}\n\n{0}\n\n{0}\n\n{0}\n\n{0}\n\n{0}",
                                                "Curabitur class aliquam vestibulum nam curae maecenas sed integer cras phasellus suspendisse quisque donec dis praesent accumsan bibendum pellentesque condimentum adipiscing etiam consequat vivamus dictumst aliquam duis convallis scelerisque est parturient ullamcorper aliquet fusce suspendisse nunc hac eleifend amet blandit facilisi condimentum commodo scelerisque faucibus aenean ullamcorper ante mauris dignissim consectetuer nullam lorem vestibulum habitant conubia elementum pellentesque morbi facilisis arcu sollicitudin diam cubilia aptent vestibulum auctor eget dapibus pellentesque inceptos leo egestas interdum nulla consectetuer suspendisse adipiscing pellentesque proin lobortis sollicitudin augue elit mus congue fermentum parturient fringilla euismod feugiat");

            var group1 = new SampleDataGroup("PrePaid",
                                             "PrePaid",
                                             "Group Subtitle: 1",
                                             "Assets/DarkGray.png",
                                             "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
            group1.Items.Add(new SampleDataItem("7ala",
                                                "7ala",
                                                "7ala",
                                                "/Assets/Prepaid/0001.png",
                                                "Enjoy the same low minute rate for all national calls no matter when or whom you call, with discounted international rates and specialdiscount on your international favorite number",
                                                ITEM_CONTENT,
                                                group1));
            group1.Items.Add(new SampleDataItem("7ala Plus",
                                                "7ala Plus",
                                                "7ala Plus",
                                                "/Assets/Prepaid/0002.png",
                                                "A new prepaid package for life. For any recharged amount equal to 10 SAR or above, customers will receive a 100% free bounus amount that will be equal to the recharged amount",
                                                ITEM_CONTENT,
                                                group1));
            group1.Items.Add(new SampleDataItem("Visitor Line 'Rihal'",
                                                "Visitor Line 'Rihal'",
                                                "Visitor Line 'Rihal'",
                                                "/Assets/Prepaid/0003.png",
                                                "  In addition to simple and low international call rates you benefit from a flat perpaid minute rate in Saudi Arabia: 0.95 SR per minute for all national calls at any time.",
                                                ITEM_CONTENT,
                                                group1));
            group1.Items.Add(new SampleDataItem("Blue wave",
                                                "Blue wave",
                                                "Blue wave",
                                                "/Assets/Prepaid/0004.png",
                                                " Specially designed for Al-Hilal fans, featuring special rates among the fans and free and free offers after each game won by Al Hilal and subscriptions to Mibily Al Hilal channels. Enjoy the reduced calling rates of 99 halalah to any Globe number, and 1.4 SR to your favorite non-Globe number in the Philippines.  Enjoy Tagalog language support, integration with SSS, call me feature, Filipino ring back tones and more, only with “Mabuhay Kababayan”. Visit the nearest Mobily shop now to get Mabuhay Kababayan SIM.",
                                                ITEM_CONTENT,
                                                group1));
            group1.Items.Add(new SampleDataItem("Fallah",
                                                "Fallah",
                                                "Fallah",
                                                "/Assets/Prepaid/0005.png",
                                                "A life-style package for youngsters with very attractive rates on calls, messaging, downloads, subscriptions to channels. Dicover a new world for you!",
                                                ITEM_CONTENT,
                                                group1));
            group1.Items.Add(new SampleDataItem("Manuhay",
                                                "Manuhay",
                                                "Manuhay",
                                                "/Assets/Prepaid/0006.png",
                                                "A prepaid mobile traiff to take you back to Philippines, to your family, friends and neighbors. Share every moment with you loved ones at the best rate to Philippines … Mabuhay!",
                                                ITEM_CONTENT,
                                                group1));
          
            this.ItemGroups.Add(group1);


            var group2 = new SampleDataGroup("PostPaid",
                                           "PostPaid",
                                           "Group Subtitle: 1",
                                           "Assets/DarkGray.png",
                                           "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
            group2.Items.Add(new SampleDataItem("7ala",
                                                "7ala",
                                                "7ala",
                                                "/Assets/Postpaid/0001.png",
                                                "Enjoy the same low minute rate for all national calls no matter when or whom you call, with discounted international rates and specialdiscount on your international favorite number",
                                                ITEM_CONTENT,
                                                group2));
            group2.Items.Add(new SampleDataItem("7ala Plus",
                                                "7ala Plus",
                                                "7ala Plus",
                                                "/Assets/Postpaid/0002.png",
                                                "A new prepaid package for life. For any recharged amount equal to 10 SAR or above, customers will receive a 100% free bounus amount that will be equal to the recharged amount",
                                                ITEM_CONTENT,
                                                group2));
            group2.Items.Add(new SampleDataItem("Visitor Line 'Rihal'",
                                                "Visitor Line 'Rihal'",
                                                "Visitor Line 'Rihal'",
                                                "/Assets/Postpaid/0003.png",
                                                "  In addition to simple and low international call rates you benefit from a flat perpaid minute rate in Saudi Arabia: 0.95 SR per minute for all national calls at any time.",
                                                ITEM_CONTENT,
                                                group2));
            group2.Items.Add(new SampleDataItem("Blue wave",
                                                "Blue wave",
                                                "Blue wave",
                                                "/Assets/Postpaid/0004.png",
                                                " Specially designed for Al-Hilal fans, featuring special rates among the fans and free and free offers after each game won by Al Hilal and subscriptions to Mibily Al Hilal channels. Enjoy the reduced calling rates of 99 halalah to any Globe number, and 1.4 SR to your favorite non-Globe number in the Philippines.  Enjoy Tagalog language support, integration with SSS, call me feature, Filipino ring back tones and more, only with “Mabuhay Kababayan”. Visit the nearest Mobily shop now to get Mabuhay Kababayan SIM.",
                                                ITEM_CONTENT,
                                                group2));
            group2.Items.Add(new SampleDataItem("Fallah",
                                                "Fallah",
                                                "Fallah",
                                                "/Assets/Postpaid/0005.png",
                                                "A life-style package for youngsters with very attractive rates on calls, messaging, downloads, subscriptions to channels. Dicover a new world for you!",
                                                ITEM_CONTENT,
                                                group2));
            group2.Items.Add(new SampleDataItem("Manuhay",
                                                "Manuhay",
                                                "Manuhay",
                                                "/Assets/Postpaid/0006.png",
                                                "A prepaid mobile traiff to take you back to Philippines, to your family, friends and neighbors. Share every moment with you loved ones at the best rate to Philippines … Mabuhay!",
                                                ITEM_CONTENT,
                                                group2));

            this.ItemGroups.Add(group2);

            var group3 = new SampleDataGroup("Services",
                                           "Services",
                                           "Group Subtitle: 1",
                                           "Assets/DarkGray.png",
                                           "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
            group3.Items.Add(new SampleDataItem("7ala",
                                                "7ala",
                                                "7ala",
                                                "/Assets/Services/0001.png",
                                                "Enjoy the same low minute rate for all national calls no matter when or whom you call, with discounted international rates and specialdiscount on your international favorite number",
                                                ITEM_CONTENT,
                                                group3));
            group3.Items.Add(new SampleDataItem("7ala Plus",
                                                "7ala Plus",
                                                "7ala Plus",
                                                "/Assets/Services/0002.png",
                                                "A new prepaid package for life. For any recharged amount equal to 10 SAR or above, customers will receive a 100% free bounus amount that will be equal to the recharged amount",
                                                ITEM_CONTENT,
                                                group3));
            group3.Items.Add(new SampleDataItem("Visitor Line 'Rihal'",
                                                "Visitor Line 'Rihal'",
                                                "Visitor Line 'Rihal'",
                                                "/Assets/Services/0003.png",
                                                "  In addition to simple and low international call rates you benefit from a flat perpaid minute rate in Saudi Arabia: 0.95 SR per minute for all national calls at any time.",
                                                ITEM_CONTENT,
                                                group3));
            group3.Items.Add(new SampleDataItem("Blue wave",
                                                "Blue wave",
                                                "Blue wave",
                                                "/Assets/Services/0004.png",
                                                " Specially designed for Al-Hilal fans, featuring special rates among the fans and free and free offers after each game won by Al Hilal and subscriptions to Mibily Al Hilal channels. Enjoy the reduced calling rates of 99 halalah to any Globe number, and 1.4 SR to your favorite non-Globe number in the Philippines.  Enjoy Tagalog language support, integration with SSS, call me feature, Filipino ring back tones and more, only with “Mabuhay Kababayan”. Visit the nearest Mobily shop now to get Mabuhay Kababayan SIM.",
                                                ITEM_CONTENT,
                                                group3));
            group3.Items.Add(new SampleDataItem("Fallah",
                                                "Fallah",
                                                "Fallah",
                                                "/Assets/Services/0005.png",
                                                "A life-style package for youngsters with very attractive rates on calls, messaging, downloads, subscriptions to channels. Dicover a new world for you!",
                                                ITEM_CONTENT,
                                                group3));
            group3.Items.Add(new SampleDataItem("Manuhay",
                                                "Manuhay",
                                                "Manuhay",
                                                "/Assets/Services/0006.png",
                                                "A prepaid mobile traiff to take you back to Philippines, to your family, friends and neighbors. Share every moment with you loved ones at the best rate to Philippines … Mabuhay!",
                                                ITEM_CONTENT,
                                                group3));

            this.ItemGroups.Add(group3);

        }
    }
}