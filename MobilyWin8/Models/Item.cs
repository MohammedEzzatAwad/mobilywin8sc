﻿using MobilyWin8.Common;

namespace MobilyWin8.Models
{
    public class Item : BindableBase
    {
        protected string m_refName;
        protected string m_refTitle;
        protected string m_refLogo;
        protected string m_refLink;
        protected string m_refCategory;
        protected string m_refDescription;

        protected int m_refIndex;


        public string Name
        {
            get { return this.m_refName; }
            set { SetProperty(ref m_refName, value); }
        }


        public string Title
        {
            get { return this.m_refTitle; }
            set { SetProperty(ref m_refTitle, value); }
        }

        public string Link
        {
            get { return this.m_refLink; }
            set { SetProperty(ref m_refLink, value); }

        }

        public string Logo
        {
            get { return this.m_refLogo; }
            set { SetProperty(ref m_refLogo, value); }
        }


        public string Category
        {
            get { return this.m_refCategory; }
            set { SetProperty(ref m_refCategory, value); }
        }

        public string Description
        {
            get { return this.m_refDescription; }
            set { SetProperty(ref m_refDescription, value); }
        }

        public int Index
        {
            get { return this.m_refIndex; }
            set { SetProperty(ref m_refIndex, value); }
        
        }


    }
}