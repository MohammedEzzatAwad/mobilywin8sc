﻿using Windows.UI.Xaml.Media.Imaging;

namespace MobilyWin8.Models
{
    public class NewsPromotionItem
    {
        public string Id { get; set; }
        public string ShortDescEn { get; set; }
        public string ShortDescAr { get; set; }
        public string DescEn { get; set; }
        public string DescAr { get; set; }
        public BitmapImage Image { get; set; }
        public string ImageFormat { get; set; }
    }
}