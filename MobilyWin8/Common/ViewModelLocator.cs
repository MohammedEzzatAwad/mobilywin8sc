﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroIoc;
using MobilyWin8.Messaging;
using MobilyWin8.Services;
using MobilyWin8.ViewModels;

namespace MobilyWin8.Common
{
    public class ViewModelLocator
    {
        private Lazy<IContainer> container;
        public IContainer Container
        {
            get { return container.Value; }
        }

        public ViewModelLocator()
        {
            container = new Lazy<IContainer>(IoC.BuildContainer);
        }

        public INavigationService NavigationService
        {
            get { return Container.Resolve<INavigationService>(); }
        }

        public IHub Hub
        {
            get { return Container.Resolve<IHub>(); }
        }

        public IDialogService DialogService
        {
            get { return Container.Resolve<IDialogService>(); }
        }
        public ShellViewModel ShellViewModel
        {
            get { return Container.Resolve<ShellViewModel>(); }
        }

        public LogInViewModel LogInViewModel
        {
            get { return Container.Resolve<LogInViewModel>(); }
        }

        public MainViewModel MainViewModel
        {
            get { return Container.Resolve<MainViewModel>(); }
        }

        public ProductsAndServicesVM ProductsAndServicesViewModel
        {
            get { return Container.Resolve<ProductsAndServicesVM>(); }
        }

        public MyAccountViewModel MyAccountViewModel
        {
            get { return Container.Resolve<MyAccountViewModel>(); }
        }
        public AlhawaViewNewsModel AlhawaViewNewsModel
        {
            get { return Container.Resolve<AlhawaViewNewsModel>(); }
        }

        public NotificationsAndUpdatesViewModel NotificationsAndUpdatesViewModel
        {
            get { return Container.Resolve<NotificationsAndUpdatesViewModel>(); }
        }
    }
}
