﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MobilyWin8.Views
{
    public sealed partial class LogInPage : UserControl
    {
        public LogInPage()
        {
            this.InitializeComponent();

            BackButton.IsEnabled = true;
        }

         

        public void Show()
        {
            VisualStateManager.GoToState(this, "LogInOpened", true);
        }

        public void Hide()
        {
            VisualStateManager.GoToState(this, "LogInClosed", true);
        }

        public void LogIn()
        {
            VisualStateManager.GoToState(this, "LogInClosed", true);
        }
        public void LogOut()
        {
            VisualStateManager.GoToState(this, "LogInClosed", true);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void Overlay_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Hide();
        }

        private void LogInButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }
    }
}
