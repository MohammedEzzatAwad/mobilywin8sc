﻿using System.Threading.Tasks;
using System.Xml.Linq;
using MobilyWin8.Helpers;
using MobilyWin8.ViewModels;

namespace MobilyWin8.Requests
{
    public static class BalanceBillRequest
    {
        public async static Task<string> LoginToEPortal(string msg)
        {
            var postSubmitter = new PostSubmitter()
            {

                Url = Constants.request_url,
                PostItems = msg,
                Type = PostSubmitter.PostTypeEnum.Post
            };
        
            return await postSubmitter.Post();
        }

        public static string CreateXmlForBalanceBillNeqaty(string hashcode, string cstRequest)
        {
            XElement xelement = new XElement((XName)Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add((object)new XElement((XName)Constants.login_FunctionId, (object)cstRequest));
            xelement.Add((object)new XElement((XName)Constants.login_device, (object)Constants.login_device_number));
            xelement.Add((object)new XElement((XName)Constants.login_HashCode, (object)hashcode));
            xelement.Add((object)new XElement((XName)Constants.login_RequestorLanguage, (object)Constants.balance_E));
            return ((object)xelement).ToString();
        }

        public static BalanceVM GetBalanceVM(string msg)
        {
            string str = string.Empty;
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (!(xelement2.Name == (XName)Constants.login_HashCode)) ;
                    }
                }
            }
            return (BalanceVM)null;
        }
    }
}