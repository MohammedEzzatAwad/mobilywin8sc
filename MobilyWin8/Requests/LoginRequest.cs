﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Xml.Linq;
using MobilyWin8.Helpers;
using MobilyWin8.ViewModels;


namespace MobilyWin8.Requests
{
    public static class LoginRequest
    {
        public async static Task<string> LoginToEPortal(string msg)
        {
            var postSubmitter = new PostSubmitter()
            {
                Url = Constants.request_url,
                PostItems = msg,
                Type = PostSubmitter.PostTypeEnum.Post
            };
            return await postSubmitter.Post();
        }


        public static string CreateXmlForLogin(string login, string pwd)
        {
            XElement xelement = new XElement((XName)Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement((XName)Constants.login_FunctionId, (object)Constants.login_LOGIN));
            xelement.Add(new XElement((XName)Constants.login_device, (object)Constants.login_device_number));
            xelement.Add(new XElement((XName)Constants.login_UserName, login));
            xelement.Add(new XElement((XName)Constants.login_Password, pwd));
            xelement.Add(new XElement((XName)Constants.login_RequestorLanguage, (object)Constants.login_EA));
            Debug.WriteLine(((object)xelement).ToString());
            return ((object)xelement).ToString();
        }

        internal static string CreateXmlForLogout(string hashcode)
        {
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.login_LOGOUT));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return ((object)xelement).ToString();
        }
        internal static string CreateXmlForCreditTransfer(string hashcode, string recipientMSISDN, string transferredAmount)
        {
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.CREDIT_TRANSFER));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.RecipientMSISDN, Constants.RecipientMSISDN));
            xelement.Add(new XElement(Constants.TransferredAmount, Constants.TransferredAmount));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return ((object)xelement).ToString();
        }

        public static bool CreditTransferConfirmation(string msg)
        {
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.ConfirmationKey)
                        {
                            if (xelement2.Value == "1001")
                                return true;
                        }
                    }
                }
            }
            return false;
        }

        public static string CreateXmlForBalanceBillNeqaty(string hashcode, string cstRequest)
        {
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, cstRequest));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.balance_E));
            return ((object)xelement).ToString();
        }

        public static PersonVM GetPersonVM(string msg)
        {
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.login_HashCode)
                            return new PersonVM(xelement2.Value);
                    }
                }
            }
            return (PersonVM)null;
        }
    }
}