﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MobilyWin8.Helpers;

namespace MobilyWin8.Requests
{
    public static class NewsPromotionRequest
    {
        public async static Task<string> LoginToEPortal(string msg)
        {
            var postSubmitter = new PostSubmitter()
            {

                Url = Constants.request_url,
                PostItems = msg,
                Type = PostSubmitter.PostTypeEnum.Post
            };

            return await postSubmitter.Post();
        }

        public static string CreateXmlForNewsPromotion(string hashcode, string cstRequest, string type)
        {
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, cstRequest));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.NewsPromotion_Id), "1");
            xelement.Add(new XElement(Constants.NewsPromotion_Type, type));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.balance_E));
            return (xelement).ToString();
        }
    }
}
