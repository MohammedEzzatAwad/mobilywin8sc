﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Xml.Linq;
using MobilyWin8.ViewModels;
using Windows.Foundation;

namespace MobilyWin8.Requests
{
    public class LogInRequester
    {
        private static HttpClient _httpClient;
        public HttpClient Instance
        {
            get
            {
                if (_httpClient == null) 
                    _httpClient = new HttpClient();

                return _httpClient;
            }
        }


        public async void LogIn(string userName, string password)
        {


            var logInMessage = CreateXmlForLogin(userName, password);

            HttpContent content = new StringContent(logInMessage);
            HttpResponseMessage message1 = new HttpResponseMessage();
            
            message1 = await Instance.PostAsync(Constants.request_url, content).AsAsyncOperation();

             

            byte[] response = await message1.Content.ReadAsByteArrayAsync();
            string message2 = new UTF8Encoding().GetString(response, 0, response.Length);

            var person = GetPersonVM(message2);

            var billMessage = CreateXmlForBalanceBillNeqaty(person.HashCode, Constants.balancebill_GET_BILL_INFO);

            

        }

        public static string CreateXmlForLogin(string login, string pwd)
        {
            XElement xelement = new XElement((XName)Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement((XName)Constants.login_FunctionId, (object)Constants.login_LOGIN));
            xelement.Add(new XElement((XName)Constants.login_device, (object)Constants.login_device_number));
            xelement.Add(new XElement((XName)Constants.login_UserName, login));
            xelement.Add(new XElement((XName)Constants.login_Password, pwd));
            xelement.Add(new XElement((XName)Constants.login_RequestorLanguage, (object)Constants.login_EA));
            Debug.WriteLine(((object)xelement).ToString());
            return ((object)xelement).ToString();
        }
        public static string CreateXmlForBalanceBillNeqaty(string hashcode, string cstRequest)
        {
            XElement xelement = new XElement((XName)Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add((object)new XElement((XName)Constants.login_FunctionId, (object)cstRequest));
            xelement.Add((object)new XElement((XName)Constants.login_device, (object)Constants.login_device_number));
            xelement.Add((object)new XElement((XName)Constants.login_HashCode, (object)hashcode));
            xelement.Add((object)new XElement((XName)Constants.login_RequestorLanguage, (object)Constants.balance_E));
            return ((object)xelement).ToString();
        }
        public static PersonVM GetPersonVM(string msg)
        {
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.login_HashCode)
                            return new PersonVM(xelement2.Value);
                    }
                }
            }
            return (PersonVM)null;
        }
    }
}