﻿using System;
using System.IO;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace MobilyWin8.Helpers
{
    public static class ImageDecodingHelper
    {
         
        /// <summary>
        /// Creates a new image from the given base64 encoded string.
        /// </summary>
        /// <param name="base64String">The encoded image data as a string.</param>
        public static BitmapImage ImageFromBase64String(this string base64String)
        {
            using (MemoryStream stream = new MemoryStream(
                Convert.FromBase64String(base64String)))
            {

                BitmapImage image = new BitmapImage();
                image.SetSource(stream.AsRandomAccessStream());
                return image;
            }
        }
    }
}
