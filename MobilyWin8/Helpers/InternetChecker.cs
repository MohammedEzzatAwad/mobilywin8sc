﻿using Windows.Data.Xml.Dom;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.Notifications;

namespace MobilyWin8.Helpers
{
    /// <summary>
    /// check the internt connection
    /// </summary>
   public class InternetChecker
    {
       

     /// <summary>
     /// detect the internet connection
     /// </summary>
     /// <returns></returns>
        public static bool IsConnectedToInternet()
        {

            bool isConnected = false;
            
            ConnectionProfile InternetConnectionProfile = NetworkInformation.GetInternetConnectionProfile();

            ToastTemplateType toastTemplate = ToastTemplateType.ToastText01;

            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);

            XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");


            if ( ApplicationData.Current.LocalSettings.Values["InternetAllowed"]== null ||(bool) ApplicationData.Current.LocalSettings.Values["InternetAllowed"] == true )
            {
                try
                {
                    var mm = InternetConnectionProfile.GetNetworkConnectivityLevel();
                    // constraind internet needs test
                    if (mm == NetworkConnectivityLevel.ConstrainedInternetAccess || mm == NetworkConnectivityLevel.InternetAccess)
                    {
                      
                        isConnected = true;
                        ApplicationData.Current.LocalSettings.Values["InternetState"] = mm.ToString();
                        toastTextElements[0].AppendChild(toastXml.CreateTextNode(mm.ToString()));
                       //  toastTextElements[0].AppendChild(toastXml.CreateTextNode((mm.ToString())));
                    }
                    else
                    {
                        toastTextElements[0].AppendChild(toastXml.CreateTextNode(mm.ToString()));

                      //  toastTextElements[0].AppendChild(toastXml.CreateTextNode((mm.ToString())));
                        ApplicationData.Current.LocalSettings.Values["InternetState"] = mm.ToString();
                    }



                }
                catch (System.Exception)
                {
                    toastTextElements[0].AppendChild(toastXml.CreateTextNode("No Internet Connection"));
                   // toastTextElements[0].AppendChild(toastXml.CreateTextNode(("NoInternetConnection")));
                    isConnected = false;
                    ApplicationData.Current.LocalSettings.Values["InternetState"] = "NoInternetConnection";
                }
            }
            else
            {
              // toastTextElements[0].AppendChild(toastXml.CreateTextNode(("InternetAccessDisabled")));
                toastTextElements[0].AppendChild(toastXml.CreateTextNode("InternetAccessDisabled"));
                ApplicationData.Current.LocalSettings.Values["InternetState"] = "InternetAccessDisabled";
                isConnected = false;
            }


             

           


            return isConnected;
        }

    }
}
