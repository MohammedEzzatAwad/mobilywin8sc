﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;

namespace MobilyWin8.Helpers
{
    public class PostSubmitter
    {
        private string m_url = string.Empty;
        private string m_values = string.Empty;
        private PostSubmitter.PostTypeEnum m_type = PostSubmitter.PostTypeEnum.Get;
 
        public string Url
        {
            get
            {
                return this.m_url;
            }
            set
            {
                this.m_url = value;
            }
        }

        public string PostItems
        {
            get
            {
                return this.m_values;
            }
            set
            {
                this.m_values = value;
            }
        }

        public PostSubmitter.PostTypeEnum Type
        {
            get
            {
                return this.m_type;
            }
            set
            {
                this.m_type = value;
            }
        }

        public PostSubmitter()
        {
        }

        public PostSubmitter(string url)
            : this()
        {
            this.m_url = url;
        }

        public PostSubmitter(string url, string value)
            : this(url)
        {
            this.m_values = value;
        }

        public async Task<string> Post()
        {
            bool flag = false;
            string str = string.Empty;
            //while (!flag)
            //{
            str = await PostData(this.m_url, m_values);

            //str = await HttpPOST(this.m_url, m_values);
                //if (str.Substring(0, 3).Equals("###"))
                //{
                //    flag = false;
                //    new ManualResetEvent(false).WaitOne(5000);
                //}
                //else
                //    flag = true;
            //}
            return str;
        }

        public string Post(string url)
        {
            this.m_url = url;
            return this.Post().Result;
        }

        public string Post(string url, string values)
        {
            this.m_values = values;
            return this.Post(url);
        }

        //private async Task<string> PostData(string url, string postData)
        //{
        //    HttpWebRequest httpWebRequest;
        //    HttpMessageHandler
        //    if (this.m_type == PostSubmitter.PostTypeEnum.Post)
        //    {


        //        httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
        //        httpWebRequest.Method = "POST";
        //        httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                
        //        using (Stream requestStream = await ((WebRequest)httpWebRequest).GetRequestStreamAsync())
        //        {
        //            byte[] bytes = new UTF8Encoding().GetBytes(postData);
        //            requestStream.Write(bytes, 0, bytes.Length);
        //        }
        //    }
        //    else
        //    {
        //        httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(url + "?" + postData));
        //        httpWebRequest.Method = "GET";
        //    }
        //    string str = string.Empty;
        //    try
        //    {
        //        using (var httpWebResponse = await httpWebRequest.GetResponseAsync())
        //        {
        //            using (Stream responseStream = httpWebResponse.GetResponseStream())
        //            {
        //                using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
        //                    str = streamReader.ReadToEnd();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("ATTENTION ! " + ex.Message);
        //        str = "###" + ex.Message;
        //    }
        //    return str;
        //}

        private async Task<string> PostData(string baseUrl, string data)
        {
            try
            {



                
                string ret = string.Empty;
                HttpClientHandler handler = new HttpClientHandler();
                handler.AllowAutoRedirect = true;
                handler.UseDefaultCredentials = true;
                 
                var client = new HttpClient(handler);

                //set RequestHeader (HTTP_USER_AGENT)
                client.DefaultRequestHeaders.Add("user-agent",
                    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");

                var httpRM = new HttpResponseMessage(HttpStatusCode.OK);

                string useUrl = baseUrl;





                if (Type == PostTypeEnum.Post)
                {
                    var content = new StringContent(data);
                    try
                    { 
                        httpRM = client.PostAsync(useUrl, content).Result; 
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        throw;
                    }
                }
                else if (Type == PostTypeEnum.Get)
                {
                    client.DefaultRequestHeaders.Add("Content-Type",
                        "application/x-www-form-urlencoded");
                    if (data.Length > 0)
                        useUrl += "?" + data;

                    httpRM = await client.GetAsync(useUrl);
                }

                httpRM.EnsureSuccessStatusCode();


                if (httpRM.IsSuccessStatusCode)
                {
                    byte[] response = await httpRM.Content.ReadAsByteArrayAsync();
                    ret = new UTF8Encoding().GetString(response, 0, response.Length);
                }
                else
                {
                    Debug.WriteLine("ATTENTION ! " + httpRM.StatusCode.ToString());
                }

                return ret;

            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex.Message);
                return "";
            }
        }


        //public async Task<string> HttpPOST(string url, string querystring)
        //{
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //    request.Method = "POST";
        //    request.ContentType = @"user-agent"+
        //            "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)" ; //"application/x-www-form-urlencoded"; // or whatever - application/json, etc, etc
        //    request.ContinueTimeout = int.MaxValue;
         
        //   StreamWriter requestWriter = new StreamWriter (await request.GetRequestStreamAsync());

        //    try
        //    {
        //        requestWriter.Write(querystring);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //       // requestWriter.Close();
        //        requestWriter = null;
        //    }

        //    WebResponse response = await request.GetResponseAsync();
        //    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
        //    {
        //        return sr.ReadToEnd();
        //    }
        //}


        //private async Task<string> PostData(string url, string nvc)
        //{
        //    var client = new HttpClient();

        //    client.DefaultRequestHeaders.Add("HTTP_USER_AGENT",
        //  "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
        //    HttpContent content = new StringContent(nvc);

        //    HttpContent responseContent = client.PostAsync(url, content).Result.Content;

        //    string ret = await responseContent.ReadAsStringAsync();


        //    return ret;
        //}


        //private async Task<string> PostData(string url, string nvc)
        //{
        //    HttpClientHandler handler = new HttpClientHandler();
        //    handler.AllowAutoRedirect = false;
        //    handler.UseDefaultCredentials = true;
        //    // handler.MaxRequestContentBufferSize = 104857600;
        //    var client = new HttpClient();

        //    HttpContent content = new StringContent(nvc);
        //    HttpResponseMessage httpRM = await client.PostAsync(url, content);
        //    httpRM.EnsureSuccessStatusCode();

        //    byte[] response = await httpRM.Content.ReadAsByteArrayAsync();
        //    var ret = new UTF8Encoding().GetString(response, 0, response.Length);
        //    return ret;
        //}

        //public async Task<string> PostData(string url, string content)
        //{
        //    HttpClientHandler handler = new HttpClientHandler();
        //    handler.UseDefaultCredentials = true;
        //    handler.AllowAutoRedirect = false;

        //    HttpClient client = new HttpClient(handler);

        //    HttpContent httpContent = new StringContent(content);
        //    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

        //    HttpResponseMessage response = await client.PostAsync(url, httpContent);

        //    byte[] arr = await response.Content.ReadAsByteArrayAsync();
        //    var ret = new UTF8Encoding().GetString(arr, 0, arr.Length);
        //    return ret;
        //}


        //public string PostData(string url, Dictionary<string, string> nvc, out bool succeeded)
        //{
        //    try
        //    {
        //        string result = string.Empty;
        //        HttpClient client = new HttpClient();

        //        HttpContent content = new FormUrlEncodedContent(nvc);

        //        HttpContent responseContent = client.PostAsync(url, content).Result.Content;

        //        string ret = responseContent.ReadAsStringAsync().Result;

        //        succeeded = true;
        //        return ret;
        //    }
        //    catch (Exception ex)
        //    {
        //        succeeded = false;
        //        return ex.ToString();
        //    }
        //}

        private void EncodeAndAddItem(ref StringBuilder baseRequest, string key, string dataItem)
        {
            if (baseRequest == null)
                baseRequest = new StringBuilder();
            if (baseRequest.Length != 0)
                baseRequest.Append("&");
            if (key != null && key != string.Empty)
            {
                baseRequest.Append(key);
                baseRequest.Append("=");
            }
            baseRequest.Append(dataItem);
        }

        public enum PostTypeEnum
        {
            Get,
            Post,
        }
    }
}