﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Callisto.Controls;
using MobilyWin8.ViewModels;
using MobilyWin8.Views.Settings;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI;
using Windows.UI.ApplicationSettings;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MobilyWin8
{
    public sealed partial class Shell : UserControl
    {
        private Popup _settingsPopup;
        private Rect _windowBounds;
        private double _settingsWidth = 346;
        private ShellViewModel dataContext;

        public Frame Frame
        {
            get { return MainFrame; }
        }

        public Shell()
        {
            InitializeComponent();

            Loaded += Shell_Loaded;
            KeyUp += Shell_KeyUp;

            dataContext = (ShellViewModel)this.DataContext;
           

            Window.Current.SizeChanged += OnWindowSizeChanged;
            _windowBounds = Window.Current.Bounds;
        }
        private void OnWindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            //TODO: resize narrow pane
            _windowBounds = Window.Current.Bounds;
        }
        void Shell_Loaded(object sender, RoutedEventArgs e)
        {
            RegisterSettings();

            Window.Current.SizeChanged += this.WindowSizeChanged;
        }

        private void WindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            UpdateView();
        }

        private void UpdateView()
        {
            string visualState = ApplicationView.Value.ToString();
            VisualStateManager.GoToState(this, visualState, false);
        }

        private void RegisterSettings()
        {
            SettingsPane.GetForCurrentView().CommandsRequested += (s, e) =>
            {
                if (!dataContext.IsLoggedIn)
                {
                    var LogInCommand = new SettingsCommand("LogIn", "Log In", (h) =>
                    {

                        this.LogInPage.Show();
                    });
                    e.Request.ApplicationCommands.Add(LogInCommand);
                }
                else
                {
                    var LogInCommand = new SettingsCommand("LogOut", "Log Out", (h) =>
                    {
                        //TODO: write LogOut code here
                    });
                    e.Request.ApplicationCommands.Add(LogInCommand);
                }
                
                _settingsPopup = new Popup();
                
                var aboutUsCommand = new SettingsCommand("AboutUs", "About Us", (h) =>
                    {
                        _settingsPopup.Closed += OnPopupClosed;
                        Window.Current.Activated += OnWindowActivated;
                        _settingsPopup.IsLightDismissEnabled = true;
                        _settingsPopup.Width = _settingsWidth;
                        _settingsPopup.Height = _windowBounds.Height;

                        AboutUs mypane = new AboutUs();
                        mypane.Width = _settingsWidth;
                        mypane.Height = _windowBounds.Height;

                        _settingsPopup.Child = mypane;
                        _settingsPopup.SetValue(Canvas.LeftProperty, _windowBounds.Width - _settingsWidth);
                        _settingsPopup.SetValue(Canvas.TopProperty, 0);
                        _settingsPopup.IsOpen = true;
                    });

                e.Request.ApplicationCommands.Add(aboutUsCommand);

                var privacyPolicyCommand = new SettingsCommand("PrivacyPolicy", "Privacy Policy", (h) =>
                {
                    _settingsPopup.Closed += OnPopupClosed;
                    Window.Current.Activated += OnWindowActivated;
                    _settingsPopup.IsLightDismissEnabled = true;
                    _settingsPopup.Width = _settingsWidth;
                    _settingsPopup.Height = _windowBounds.Height;

                    PrivacyPolicy mypane = new PrivacyPolicy();
                    mypane.Width = _settingsWidth;
                    mypane.Height = _windowBounds.Height;

                    _settingsPopup.Child = mypane;
                    _settingsPopup.SetValue(Canvas.LeftProperty, _windowBounds.Width - _settingsWidth);
                    _settingsPopup.SetValue(Canvas.TopProperty, 0);
                    _settingsPopup.IsOpen = true;
                });

                e.Request.ApplicationCommands.Add(privacyPolicyCommand);
            };
        }
        void OnPopupClosed(object sender, object e)
        {
            Window.Current.Activated -= OnWindowActivated;
        }
        private void OnWindowActivated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
        {
            if (e.WindowActivationState == Windows.UI.Core.CoreWindowActivationState.Deactivated)
            {
                _settingsPopup.IsOpen = false;
            }
        }
        private static VirtualKey[] alphaKeys = new[] 
            {
                VirtualKey.A, VirtualKey.B, VirtualKey.C, VirtualKey.D, VirtualKey.E,
                VirtualKey.F, VirtualKey.G, VirtualKey.H, VirtualKey.I, VirtualKey.J,
                VirtualKey.K, VirtualKey.L, VirtualKey.M, VirtualKey.N, VirtualKey.O,
                VirtualKey.P, VirtualKey.Q, VirtualKey.R, VirtualKey.S, VirtualKey.T,
                VirtualKey.U, VirtualKey.V, VirtualKey.W, VirtualKey.X, VirtualKey.Y,
                VirtualKey.Z
            };

        void Shell_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Back)
            {
                if (Frame.CanGoBack)
                {
                    Frame.GoBack();
                }
                return;
            }

            if (alphaKeys.Contains(e.Key))
            {
                //App.ViewModelLocator.Hub.Send(new ShowSearchPaneMessage());
            }
        }
    }
}
