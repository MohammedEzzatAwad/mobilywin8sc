﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NotificationsExtensions.TileContent;

namespace MobilyWin8.Adapters
{
    public interface ITileUpdateManager
    {
        void UpdatePrimaryTile(ITileNotificationContent content);
    }
}
