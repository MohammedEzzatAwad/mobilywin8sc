﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobilyWin8.Services;
using NotificationsExtensions.TileContent;
using Windows.UI.Notifications;

namespace MobilyWin8.Adapters
{
    public class TileUpdateManagerAdapter : ITileUpdateManager
    {
        public void UpdatePrimaryTile(ITileNotificationContent content)
        {
            var notification = content.CreateNotification();
            TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);
        }
    }
}
