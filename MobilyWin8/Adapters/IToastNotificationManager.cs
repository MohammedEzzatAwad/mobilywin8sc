﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NotificationsExtensions.ToastContent;

namespace MobilyWin8.Adapters
{
    public interface IToastNotificationManager
    {
        void Show(IToastNotificationContent content);
    }
}
