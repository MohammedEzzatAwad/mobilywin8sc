﻿using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;

namespace MobilyWin8.Adapters
{
    public interface IDataTransferManager
    {
        event TypedEventHandler<DataTransferManager, SettableDataRequestedEventArgs> DataRequested;
        void ShowShareUI(); 
    }
}