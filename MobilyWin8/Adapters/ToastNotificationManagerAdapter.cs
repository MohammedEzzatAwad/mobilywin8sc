﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NotificationsExtensions.ToastContent;
using Windows.UI.Notifications;

namespace MobilyWin8.Adapters
{
    public class ToastNotificationManagerAdapter : IToastNotificationManager
    {
        public void Show(IToastNotificationContent content)
        {
            var notification = content.CreateNotification();
            ToastNotificationManager.CreateToastNotifier().Show(notification);
        }
    }
}
