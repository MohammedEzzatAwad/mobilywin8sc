﻿namespace MobilyWin8.Messaging
{
    public class LogInLogOutMessage : IMessage
    {
        public bool IsLogged { get; set; }
    }
}