﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilyWin8.Messaging
{
    public interface IHub
    {
        Task Send<TMessage>(TMessage message) where TMessage : IMessage; 
    }
}
