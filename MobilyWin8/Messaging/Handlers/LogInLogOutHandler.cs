﻿using Windows.UI.ApplicationSettings;

namespace MobilyWin8.Messaging.Handlers
{
    public class LogInLogOutHandler : IHandler<LogInLogOutMessage>
    {
        public void Handle(LogInLogOutMessage message)
        {
            if(message.IsLogged)
            {
                 
                SettingsPane.GetForCurrentView().CommandsRequested -= (sender, args) =>
                    {

                    };

               
                SettingsPane.GetForCurrentView().CommandsRequested += LogInLogOutHandler_CommandsRequested;
               
            }
        }

        void LogInLogOutHandler_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            var settingsCommand = new SettingsCommand("LogOut", "Log Out", (h) =>
            {
                //TODO: wite code here
            });

            args.Request.ApplicationCommands.Add(settingsCommand);
        }
    }
}