﻿using Windows.UI.ApplicationSettings;

namespace MobilyWin8.Messaging.Handlers
{
    public class ShowSettingsHandler : IHandler<ShowSettingsMessage>
    {
        public void Handle(ShowSettingsMessage message)
        {
            SettingsPane.Show();

        } 
    }
}