﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilyWin8.Messaging
{


    public interface IAsyncHandler<TMessage> where TMessage : IMessage
    {
        Task HandleAsync(TMessage message);
    }

    public interface IHandler
    {
    }

    public interface IHandler<TMessage> : IHandler where TMessage : IMessage
    {
        void Handle(TMessage message);
    }

    public static class HandlerExtensions
    {
        public static Task HandlerAsync<TMessage>(this IHandler<TMessage> handler, TMessage message) where TMessage : IMessage
        {
            return Task.Run(() => handler.HandlerAsync(message));
        }

    }
}
