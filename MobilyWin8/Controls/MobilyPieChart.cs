﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;

namespace MobilyWin8.Controls
{
    class PieRate:WinRTXamlToolkit.Controls.PieSlice
    {
        

        static PieRate()
        { 
        
        }




        public int FirstValue
        {
            get { return (int)GetValue(FirstValueProperty); }
            set { SetValue(FirstValueProperty, value); }
        }

        //public static int SecondValue = 0;
        //public static int ThirdValue = 0;

    //    public int SecondValue { get { return second; } set {if (SecondValue>0)
    //{
    //    FirstValue = FirstValue;
    //    second = SecondValue;

    //}
    //    else
    //    {
    //        second = 1;
    //    }
            
    //        ; } }
    //    public int ThirdValue { get { return third; } set { third = ThirdValue; FirstValue = FirstValue; } }

    //    private static int second { get; set; }
    //    private static int third { get; set; }




        public int SecondValue
        {
            get { return (int)GetValue(SecondValueProperty); }
            set { SetValue(SecondValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SecondValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SecondValueProperty =
            DependencyProperty.Register("SecondValue", typeof(int), typeof(PieRate), new PropertyMetadata(100,SecondChanged));

        private static void SecondChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            //PieRate pr = (PieRate)o;
            //if ((int)e.NewValue != null)
            //{
            //    double total = (int)e.NewValue + pr.FirstValue + pr.ThirdValue;
            //    double rate = (int)e.NewValue / total;
            //    double value = rate * 360;
            //    pr.EndAngle = pr.StartAngle + value;
            //}

        }





        public int ThirdValue
        {
            get { return (int)GetValue(ThirdValueProperty); }
            set { SetValue(ThirdValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThirdValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThirdValueProperty =
            DependencyProperty.Register("ThirdValue", typeof(int), typeof(PieRate), new PropertyMetadata(100,ThirdValueChanged));


        private static void ThirdValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            //PieRate pr = (PieRate)o;
            //if ((int)e.NewValue != null)
            //{
            //    double total = (int)e.NewValue + pr.FirstValue + pr.SecondValue;
            //    double rate = (int)e.NewValue / total;
            //    double value = rate * 360;
            //    pr.EndAngle = pr.StartAngle + value;
            //}

        }


        // Using a DependencyProperty as the backing store for RadeemPoints.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FirstValueProperty =
            DependencyProperty.Register("FirstValue", typeof(int), typeof(PieRate), new PropertyMetadata(100, FirstChanged));
        
        private static void FirstChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PieRate pr = (PieRate)o;
            if ((int)e.NewValue != null)
            {
                double total = (int)e.NewValue + pr.SecondValue + pr.ThirdValue;
                double rate = (int)e.NewValue / total;
                double value = rate * 360;
                pr.EndAngle = pr.ActualStartAngel +  value;
              //  pr.StartAngle = 0;
            }
        
        }






        public int ActualStartAngel
        {
            get { return (int)GetValue(ActualStartAngelProperty); }
            set { SetValue(ActualStartAngelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActualStartAngel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActualStartAngelProperty =
            DependencyProperty.Register("ActualStartAngel", typeof(int), typeof(PieRate), new PropertyMetadata(290,AngelChanged));


        private static void AngelChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PieRate pr = (PieRate)o;
            if ((int)e.NewValue != null)
            {
                double total = (int)e.NewValue + pr.SecondValue + pr.ThirdValue;
                double rate = (int)e.NewValue / total;
                double value = rate * 360;
                pr.StartAngle = pr.ActualStartAngel;
                pr.EndAngle =pr.EndAngle+ pr.ActualStartAngel + value;
               
            }

        }

        
        //public PieRate pieRate
        //{ 
        ////RadeemPointsRate = RadeemPoints+LostPoints+CurrentBalance;
        
        //}

      
    }
}
