﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobilyWin8.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MobilyWin8.Controls
{
    public class MainTemplateSelector : DataTemplateSelector
    {
        public DataTemplate WithImageBigTemplate { get; set; }
        public DataTemplate WithImageSmallTemplate { get; set; }
        public DataTemplate LastItemTemplate { get; set; }
        

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {

            var gridViewItem = MobilyWin8.Classes.VisualTree.FindVisualParent<MainPageGridView>(container);
            var ss = gridViewItem.ItemsSource;

            var dataItem = item as Item;

            int index = dataItem.Index;

            //if (dataItem != null)
            //{
            //    foreach (var category in MainViewModel.Instance.CategoriesWithItems)
            //    {
            //        foreach (var myItem in category.Items)
            //        {
            //            if (myItem.ID == dataItem.ID)
            //            {
            //                index = category.Items.IndexOf(myItem);
            //                break;
            //            }
            //        }
            //    }
            //}




            if (index == 0)
            {
               
                    return WithImageBigTemplate;
                

            }

            if (index > 0 && index < 5)
            {
                
                    return WithImageSmallTemplate;

            }
            else
            {
                return LastItemTemplate;
            }
           


        }
    }


}
