﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobilyWin8.Models;
using MobilyWin8.ViewModels;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MobilyWin8.Controls
{
   public  class MainPageGridView:GridView
    {
 private int rowVal;
        private int colVal;
        private Random _rand;
        private List<Size> _sequence;
        
       public MainPageGridView()
        {
            _rand = new Random();
            _sequence = new List<Size> {
                LayoutSizes.PrimaryItem, 
                LayoutSizes.OtherSmallItem, LayoutSizes.OtherSmallItem, 
                LayoutSizes.OtherSmallItem, 
                LayoutSizes.OtherSmallItem, 
                LayoutSizes.OtherSmallItem, LayoutSizes.OtherSmallItem, LayoutSizes.OtherSmallItem
            };
        }

        protected override void PrepareContainerForItemOverride(Windows.UI.Xaml.DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);
            var gridViewItem = MobilyWin8.Classes.VisualTree.FindVisualParent<MainPageGridView>(element);
            var ss = gridViewItem.ItemsSource;
          
            var dataItem = item as Item;

            int index = dataItem.Index;

            //if (dataItem != null)
            //{
               

            //    foreach (var category in MainViewModel.Instance.CategoriesWithItems) 
            //    {
            //        foreach (var myItem in category.Items) 
            //        {
            //            if (myItem.ID == dataItem.ID) 
            //            {
            //               index = category.Items.IndexOf(myItem);
            //               break;
            //            }
            //        }
            //    }
            //}

           

            if (index >= 0 && index < _sequence.Count)
            {
               
                    colVal = (int)_sequence[index].Width;
                    rowVal = (int)_sequence[index].Height;
                
                

            }
            else
            {
                colVal = (int)LayoutSizes.OtherSmallItem.Width;
                rowVal = (int)LayoutSizes.OtherSmallItem.Height;

            }


            VariableSizedWrapGrid.SetRowSpan(element as UIElement, rowVal);
            VariableSizedWrapGrid.SetColumnSpan(element as UIElement, colVal);


        }
    }

    public static class LayoutSizes
    {
        public static Size OnlyItem = new Size (6,6);
        public static Size PrimaryItem = new Size(4, 4);
        public static Size SecondarySmallItem = new Size(3, 1);
        public static Size SecondaryTallItem = new Size(3, 2);
        public static Size OtherSmallItem = new Size(2, 2);

    }

    }

